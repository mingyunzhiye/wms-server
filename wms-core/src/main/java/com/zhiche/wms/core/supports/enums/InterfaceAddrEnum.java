package com.zhiche.wms.core.supports.enums;

public enum InterfaceAddrEnum {

    INBOUND_SCHECDULE_TMS("10", "/mSearchVINOrder.jspx", "TMS定时任务获取入库通知单数据接口地址"),
    SHIPMENT_SCHEDULE_TMS("20", "/mShipSearch.jspx", "TMS定时任务获取出库通知单数据接口地址"),
    UAA_SIGNUP_NORMAL("30", "/uaa/tenant/account/create", "uaa 注册用户地址"),
    UAA_UPDATE_PASSWORD("31", "/uaa/account/updatePassword", "uaa 更新用户登录密码接口地址"),
    EVENT_URI("50","/event/export","interguration提车/寻车/移车/入库/出库任务回传OTM接口地址"),
    PROCESS_RESULT_URI("51","/event/getProcessResult","interguration得到异步处理结果"),
    REGISTRATION_URL("60","/interface/adapter/abnormal_registration_sync","车辆异常信息同步推送otm是否可发"),
    VEHICLE_URL("70","/vehicleStandardMode/queryVehicleModelInfo","查询车型信息");

    private final String code;
    private final String address;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    public String getAddress() {
        return address;
    }

    InterfaceAddrEnum(String code, String address, String detail) {
        this.code = code;
        this.address = address;
        this.detail = detail;
    }

}
