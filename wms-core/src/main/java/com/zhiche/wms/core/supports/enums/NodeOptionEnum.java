package com.zhiche.wms.core.supports.enums;

public enum NodeOptionEnum {

    NODE_INBOUND_AUTO("1001","notice_auto_putaway", "自动入库节点"),
    NODE_INBOUND_INSPECT("1002","putaway_must_inspect", "入库前必须验车"),
    NODE_OUTBOUND_AUTO("2001","notice_auto_ship", "出库通知自动出库"),
    NODE_OUTBOUND_PREPARE("2002","ship_must_prepare", "出库前必须备料"),
    NODE_OUTBOUND_TRUCK("2003","shipped_auto_truckship", "出库后自动发运"),
    EXCP_NOT_SHIP("3001","excp_not_ship","异常不发运"),
    EXCP_SHIP("3002","EXCP_SHIP","异常发运");

    private final String id;
    private final String code;
    private final String detail;

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    NodeOptionEnum(String id, String code, String detail) {
        this.id = id;
        this.code = code;
        this.detail = detail;
    }
}
