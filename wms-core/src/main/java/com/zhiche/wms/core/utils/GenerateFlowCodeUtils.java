package com.zhiche.wms.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GenerateFlowCodeUtils {

    //单实例
    private static GenerateFlowCodeUtils generateFlowCodeUtils = null;

    private GenerateFlowCodeUtils() {

    }

    /**
     * 取得PrimaryGenerator的单例实现
     */
    public static GenerateFlowCodeUtils getInstance() {
        if (generateFlowCodeUtils == null) {
            synchronized (GenerateFlowCodeUtils.class) {
                if (generateFlowCodeUtils == null) {
                    generateFlowCodeUtils = new GenerateFlowCodeUtils();
                }
            }
        }
        return generateFlowCodeUtils;
    }


    /**
     * 每天开始计数
     * ASN201710300001
     *
     * @param headCode      前面标识 ASN
     * @param lastCode      上一code的编码，为空直接从0001开始 20171030
     * @param decimalFormat 生成流水号的格式 0001
     */
    public static synchronized String getNumber(String headCode, String lastCode, String decimalFormat) {
        String id = null;
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        if (StringUtils.isBlank(lastCode)) {
            //说明没有数据从0001开始
            DecimalFormat df = new DecimalFormat(decimalFormat);
            id = headCode + formatter.format(date) + df.format(1);
        } else {
            //传入非空
            String todayDate = lastCode.substring(3, 11);
            //这个判断数据取出来的最后一个业务单号是不是当天的
            if (!formatter.format(date).equals(todayDate)) {
                System.out.println("新的一天");
                todayDate = formatter.format(date);
                //如果是新的一月的就直接变成0001
                id = headCode + todayDate + "0001";
            } else {
                DecimalFormat df = new DecimalFormat(decimalFormat);
                //不是新的一月就累加
                id = headCode + formatter.format(date) + df.format(1 + Integer.parseInt(lastCode.substring(11)));
            }
        }
        return id;
    }

}
