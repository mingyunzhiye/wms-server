package com.zhiche.wms.core.supports.enums;

public enum ExceptionCodeDetailEnum {
    BASE_EXP_11("-1", "系统异常"),
    BASE_EXP_500("500", "系统异常"),
    BASE_EXP_304("304", "参数异常"),
    BASE_EXP_503("503", "请求超时");

    private final String code;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    ExceptionCodeDetailEnum(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }


}
