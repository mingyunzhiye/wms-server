package com.zhiche.wms.core.utils.qiniu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "qiNiu")
public class QiNiuConfigurationBean {

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String downloadAddress;
    private Integer invalidationTime;
    private String zoneName;

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getDownloadAddress() {
        return downloadAddress;
    }

    public void setDownloadAddress(String downloadAddress) {
        this.downloadAddress = downloadAddress;
    }

    public Integer getInvalidationTime() {
        return invalidationTime;
    }

    public void setInvalidationTime(Integer invalidationTime) {
        this.invalidationTime = invalidationTime;
    }

}

