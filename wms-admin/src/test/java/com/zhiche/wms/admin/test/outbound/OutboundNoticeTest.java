package com.zhiche.wms.admin.test.outbound;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.domain.mapper.outbound.OutboundNoticeHeaderMapper;
import com.zhiche.wms.domain.mapper.outbound.OutboundNoticeLineMapper;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundShipHeader;
import com.zhiche.wms.service.constant.ShipType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.inbound.IInboundNoticeLineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class OutboundNoticeTest {

    public Logger logger = LoggerFactory.getLogger(getClass());

    //@Autowired
    //private TestRestTemplate restTemplate;
    @Autowired
    private OutboundNoticeHeaderMapper headerMapper;
    @Autowired
    private OutboundNoticeLineMapper lineMapper;

    @Autowired
    private IInboundNoticeLineService inboundNoticeLineService;


    @Test
    public void testGetStr() {
        List<OutboundNoticeHeader> headers = headerMapper.selectList(null);
        List<OutboundNoticeLine> lines = lineMapper.selectList(null);
        String hStr = JSONObject.toJSONString(headers);
        String lStr = JSONObject.toJSONString(lines);
    }

    @Test
    public void pageNoticeLines() {

        Page<InboundNoticeLine> page = new Page<>();
        page.setCurrent(1);
        page.setSize(10);

        EntityWrapper<InboundNoticeLine> ew = new EntityWrapper<>();
        ew.like("lot_no0","038").or().like("lot_no1","038");
//        Page<OutboundNoticeLine> page1 = inboundNoticeLineService.selectPage(page,ew);
        Page<InboundNoticeLine> page1 = inboundNoticeLineService.selectPage(page,ew);
        logger.info("行数：{}",page1.getRecords().size());
        logger.info("数据：{}",page1.getRecords().get(0));

    }

}
