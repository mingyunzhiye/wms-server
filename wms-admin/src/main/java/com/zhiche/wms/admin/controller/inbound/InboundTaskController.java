package com.zhiche.wms.admin.controller.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.admin.vo.InboundVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.inbound.InboundDTO;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.inbound.IInboundNoticeLineService;
import com.zhiche.wms.service.inbound.IInboundPutawayHeaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 入库计划
 */
@RestController
@RequestMapping("/inboundTask")
public class InboundTaskController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundTaskController.class);
    @Autowired
    private IInboundNoticeLineService inboundNoticeLineService;
    @Autowired
    private IInboundPutawayHeaderService iinboundPutawayHeaderService;

    /**
     * 加载分页结果，根据条件进行分页查询
     *
     * @param page 封装的分页条件
     * @return 分页结果集
     */
    @PostMapping("/queryPage")
    public RestfulResponse<Page<InboundDTO>> loadPageList(@RequestBody Page<InboundDTO> page) {
        RestfulResponse<Page<InboundDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            restfulResponse.setData(inboundNoticeLineService.selectInboundTask(page));
        } catch (BaseException e) {
            //对自定义异常信息的处理
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            //对controller中的非自定义异常做处理
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 入库确认--admin 支持批量入库
     */
    @PostMapping("/putwayList")
    public RestfulResponse<List<String>> putwayList(@RequestBody InboundVo inboundTaskVo) {
        RestfulResponse<List<String>> restfulResponse = new RestfulResponse<>(0, "入库成功", null);
        try {
            List<String> data = inboundNoticeLineService.updateInboundConfirm(inboundTaskVo.getLineIds(), inboundTaskVo.getHouseId());
            if(CollectionUtils.isNotEmpty(data)){
                restfulResponse.setCode(-2);
                restfulResponse.setMessage("处理结果");
                restfulResponse.setData(data);
            }
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 查询入库明细-扫码
     */
    @PostMapping("/seachDetail")
    public RestfulResponse<InboundDTO> seachDetail(@RequestBody InboundVo inboundVo) {
        RestfulResponse<InboundDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            restfulResponse.setData(inboundNoticeLineService.inboundDetailByVinNo(inboundVo.getKey(), inboundVo.getHouseId()));
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    /**
     * 扫码入库-admin
     */
    @PostMapping("/inboundPutWay")
    public RestfulResponse<InboundDTO> inboundPutWay(@RequestBody InboundDTO inboundDTO) {
        RestfulResponse<InboundDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            iinboundPutawayHeaderService.updateNoticeLineWithLocationId(inboundDTO.getId(), PutAwayType.NOTICE_PUTAWAY,
                    SourceSystem.HAND_MADE, inboundDTO.getLocationId()).getInboundPutawayLineList().get(0).getId();
            //查询分配的入库单
            inboundNoticeLineService.inboundDetailByVinNo(inboundDTO.getLotNo1(), inboundDTO.getStoreHouseId());
            restfulResponse.setData(inboundDTO);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/queryArea")
    public RestfulResponse<List<StoreAreaAndLocationDTO>> queryUsableLocation(@RequestBody InboundVo inboundVo) {
        RestfulResponse<List<StoreAreaAndLocationDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            restfulResponse.setData(inboundNoticeLineService.queryStoreLocation(inboundVo.getHouseId()));
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        LOGGER.info("map" + restfulResponse.toString());
        return restfulResponse;
    }

    @PostMapping("/printBarcode")
    public RestfulResponse<String> getPrintBarcode(@RequestBody InboundVo inboundVo) {
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            String barcode = inboundNoticeLineService.getBarcode(inboundVo.getKey());
            restfulResponse.setData(barcode);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        LOGGER.info("map" + restfulResponse.toString());
        return restfulResponse;
    }
}
