package com.zhiche.wms.admin.controller.sys;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.admin.vo.sys.RolePermRequestVO;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.sys.Permission;
import com.zhiche.wms.domain.model.sys.Role;
import com.zhiche.wms.service.sys.IRoleService;
import com.zhiche.wms.service.sys.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2017/12/7.
 */

@RestController
@RequestMapping(value = "/role", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IUserService userService;


    /**
     * 分页查询 -角色
     */
    @PostMapping(value = "/queryPage", consumes = "application/json")
    public RestfulResponse<Page<Role>> queryRoleList(@RequestBody Page<Role> page) {
        RestfulResponse<Page<Role>> result = new RestfulResponse<>(0, "success");
        try {
            result.setData(roleService.queryRolePage(page));
        } catch (BaseException be) {
            result.setMessage(be.getMessage());
            result.setCode(-1);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("系统异常");
        }
        return result;
    }

    /**
     * 新建角色
     */
    @PostMapping(value = "/create", consumes = "application/json")
    public RestfulResponse<Role> createRole(@RequestBody Role role, BindingResult bindingResult) {
        RestfulResponse<Role> result = new RestfulResponse<>(0, "success");
        //参数校验
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            result.setCode(-1);
            StringBuilder stringBuilder = new StringBuilder();
            for (ObjectError error : errors) {
                stringBuilder.append(error.getDefaultMessage());
            }
            result.setMessage(stringBuilder.toString());
            result.setCode(-1);
            return result;
        }
        //业务处理
        try {
            //            User user = getUser();
//            User user = userService.selectById(1);
            if (roleService.insert(role)) {
                result.setData(role);
            }

        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }

    @PostMapping(value = "/update", consumes = "application/json")
    public RestfulResponse<Integer> updateRole(@RequestBody Role role) {
        RestfulResponse<Integer> result = new RestfulResponse<>(0, "success");
        try {
            if (roleService.updateById(role)) {
                result.setData(1);
            }
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }

    @PostMapping(value = "/delete", consumes = "application/json")
    public RestfulResponse<Integer> deleteRole(@RequestBody List<Integer> ids) {
        RestfulResponse<Integer> result = new RestfulResponse<>(0, "success");
        try {
            if (roleService.deleteRole(ids)) {
                result.setData(1);
            }
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }

    @GetMapping(value = "/permission/own")
    public RestfulResponse<List<Integer>> getOwnPermissionIds(@RequestParam("roleId") Integer roleId) {
        RestfulResponse<List<Integer>> result = new RestfulResponse<>(0, "success");
        try {
            List<Permission> ownPermission = roleService.listOwnPermission(roleId);
            if (!Objects.equals(ownPermission, null)) {
                List<Integer> permissionIds = new ArrayList<>();
                for (Permission permission : ownPermission) {
                    permissionIds.add(permission.getId());
                }
                result.setData(permissionIds);
            }
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @GetMapping(value = "/permission/ownAll")
    public RestfulResponse<List<Integer>> getOwnAllPermissionIds(@RequestParam("roleId") Integer roleId) {
        RestfulResponse<List<Integer>> result = new RestfulResponse<>(0, "success");
        try {
            List<Permission> ownPermission = roleService.listOwnAllPermission(roleId);
            if (!Objects.equals(ownPermission, null)) {
                List<Integer> permissionIds = new ArrayList<>();
                for (Permission permission : ownPermission) {
                    permissionIds.add(permission.getId());
                }
                result.setData(permissionIds);
            }
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @GetMapping(value = "/permission/ownBottomPermission")
    public RestfulResponse<List<Integer>> getOwnBottomPermissionIds(@RequestParam("roleId") Integer roleId) {
        RestfulResponse<List<Integer>> result = new RestfulResponse<>(0, "success");
        try {

            List<Permission> bottomPermission = roleService.getOwnBottomPermission(roleId);

            if (!Objects.equals(bottomPermission, null)) {
                List<Integer> bottomPermissionIds = new ArrayList<>();
                for (Permission permission : bottomPermission) {
                    bottomPermissionIds.add(permission.getId());
                }
                result.setData(bottomPermissionIds);
            }
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @PostMapping(value = "/permission/update", consumes = "application/json")
    public RestfulResponse<Integer> updateRolePermission(@RequestBody RolePermRequestVO reVO) {
        RestfulResponse<Integer> result = new RestfulResponse<>(0, "success");
        try {
            int i = roleService.assignPermission(reVO.getRoleId(), reVO.getPermissionIds());
            result.setData(i);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }

    @GetMapping(value = "/all")
    public RestfulResponse<List<Role>> listAll() {
        RestfulResponse<List<Role>> result = new RestfulResponse<>(0, "success");
        try {
            result.setData(roleService.selectList(null));
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }
}
