package com.zhiche.wms.admin.surpports.schedule;

import com.zhiche.wms.service.log.IItfImplogHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * wms和TMS 系统对接的接口
 */
@Component
public class InboundSchedule {

    @Autowired
    private IItfImplogHeaderService logHeaderService;

    @Scheduled(cron = "0 1/5 * * * ?")
    public void inboundDataFromTMSBySchedule() {
        logHeaderService.saveInboundDataFromTMSBySchedule();
    }
}
