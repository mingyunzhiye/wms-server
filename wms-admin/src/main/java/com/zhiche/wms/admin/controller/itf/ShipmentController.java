package com.zhiche.wms.admin.controller.itf;

import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.log.ItfImplogHeader;
import com.zhiche.wms.service.dto.ShipmentDTO;
import com.zhiche.wms.service.log.IItfImplogHeaderService;
import com.zhiche.wms.service.utils.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zhaoguixin on 2018/6/12.
 */
@RestController
@RequestMapping(value = "/shipment")
public class ShipmentController {

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private IItfImplogHeaderService iItfImplogHeaderService;

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public RestfulResponse shipmentImport(@RequestBody String shipment) {
        logger.info("controller:/shipment/import data: {}", shipment);
        RestfulResponse result = new RestfulResponse<>(0, "成功", null);
        try {
            iItfImplogHeaderService.otmShipmentImport(shipment);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public RestfulResponse<String> shipmentExport(@RequestBody String shipment) {
        logger.info("controller:/shipment/export data: {}", shipment);
        RestfulResponse<String> result = new RestfulResponse<>(0, "成功", null);
        try {
            //发送 POST 请求
            String sr = HttpRequest.sendPost("http://222.126.229.158:7778/GC3/glog.integration.servlet.WMServlet", shipment);
            logger.info("export result:{}", sr);
            result.setData(sr);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public RestfulResponse exportResult(@RequestBody String resultXml) {
        logger.info("controller:/shipment/result data: {}", resultXml);
        RestfulResponse result = new RestfulResponse<>(0, "成功", null);
        try {
            ItfImplogHeader itfImplogHeader = new ItfImplogHeader();
            itfImplogHeader.setId(snowFlakeId.nextId());
            itfImplogHeader.setType("OTM回调");
            itfImplogHeader.setDataContent(resultXml);
            iItfImplogHeaderService.insert(itfImplogHeader);
            logger.info(resultXml);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    /**
     * 统一从integration接收运单指令
     */
    @PostMapping(value = "/recvShipment")
    public RestfulResponse recvShipment(@RequestBody ShipmentDTO shipmentDTO) {
        logger.info("controller:/shipment/recvShipment data: {}", shipmentDTO);
        RestfulResponse result = new RestfulResponse<>(0, "成功", null);
        try {
            iItfImplogHeaderService.updateShipmentOTM(shipmentDTO);
        } catch (BaseException e) {
            logger.error("controller:/shipment/recvShipment BaseException:{}", e);
            result.setCode(-1);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("controller:/shipment/recvShipment error:{}", e);
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}
