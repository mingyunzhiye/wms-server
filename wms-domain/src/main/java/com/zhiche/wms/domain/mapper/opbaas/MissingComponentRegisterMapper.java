package com.zhiche.wms.domain.mapper.opbaas;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.domain.model.opbaas.MissingComponentRegister;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 缺件登记 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-07
 */
public interface MissingComponentRegisterMapper extends BaseMapper<MissingComponentRegister> {

    List<MissingComponentRegister> queryMissing(@Param("ew") Wrapper<MissingRegidterDetailDTO> ew);

    List<String> queryComponentLevel2(@Param("ew") Wrapper<MissingRegidterDetailDTO> ew);
}
