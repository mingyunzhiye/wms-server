package com.zhiche.wms.domain.mapper.log;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.log.ItfExplogLine;

/**
 * <p>
 * 接口导出日志明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface ItfExplogLineMapper extends BaseMapper<ItfExplogLine> {

}
