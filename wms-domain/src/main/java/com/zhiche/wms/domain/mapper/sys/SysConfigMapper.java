package com.zhiche.wms.domain.mapper.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.sys.SysConfig;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:00 2018/12/12
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
