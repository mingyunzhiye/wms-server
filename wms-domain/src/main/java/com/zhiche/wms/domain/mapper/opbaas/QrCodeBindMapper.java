package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.opbaas.OpQrCodeBind;

/**
 * <p>
 * 二维码绑定 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface QrCodeBindMapper extends BaseMapper<OpQrCodeBind> {

}
