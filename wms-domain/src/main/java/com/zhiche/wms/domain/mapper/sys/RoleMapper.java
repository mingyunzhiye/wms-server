package com.zhiche.wms.domain.mapper.sys;

import com.zhiche.wms.domain.model.sys.Permission;
import com.zhiche.wms.domain.model.sys.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface RoleMapper extends BaseMapper<Role> {
    List<Permission> selectRolePermission(@Param("roleId") Integer roleId);

    List<Permission> selectOwnBottomPermission(@Param("roleId") Integer roleId);
}
