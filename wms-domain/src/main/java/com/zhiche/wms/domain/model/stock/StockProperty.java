package com.zhiche.wms.domain.model.stock;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by zhaoguixin on 2018/5/30.
 */
public class StockProperty extends SkuProperty implements Serializable {

    private static final long serialVersionUID = -657217159324282589L;

    /**
     * 库存ID
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long stockId;

    /**
     * 仓库
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 储位
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long locationId;

    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 净重
     */
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    private BigDecimal grossWeight;
    /**
     * 体积
     */
    private BigDecimal grossCubage;
    /**
     * 件数
     */
    private BigDecimal packedCount;

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }


    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }


    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage() {
        return grossCubage;
    }

    public void setGrossCubage(BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount() {
        return packedCount;
    }

    public void setPackedCount(BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    @Override
    public String toString() {
        return "Stock{" +
                ", stockId=" + stockId +
                ", storeHouseId=" + storeHouseId +
                ", locationId=" + locationId +
                super.toString() +
                ", qty=" + qty +
                ", netWeight=" + netWeight +
                ", grossWeight=" + grossWeight +
                ", grossCubage=" + grossCubage +
                ", packedCount=" + packedCount +
                "}";
    }
}
