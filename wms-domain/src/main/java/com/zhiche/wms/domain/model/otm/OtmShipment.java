package com.zhiche.wms.domain.model.otm;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 调度指令
 * </p>
 *
 * @author qichao
 * @since 2018-06-13
 */
@TableName("otm_shipment")
public class OtmShipment extends Model<OtmShipment> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<OtmOrderRelease> otmOrderReleaseList;

    /**
     * ID
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 调度指令ID
     */
    @TableField("shipment_gid")
    private String shipmentGid;
    /**
     * 运输指令类型
     */
    @TableField("shipment_type")
    private String shipmentType;
    /**
     * 数据处理方式(I:新增,U:更新,D:删除)
     */
    @TableField("transaction_code")
    private String transactionCode;
    /**
     * 运输方式编码
     */
    @TableField("transport_mode_gid")
    private String transportModeGid;
    /**
     * 分供方编码
     */
    @TableField("service_provider_gid")
    private String serviceProviderGid;
    /**
     * 分供方名称
     */
    @TableField("service_provider_name")
    private String serviceProviderName;
    /**
     * 司机编码
     */
    @TableField("driver_gid")
    private String driverGid;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 车牌号/车厢号/船号
     */
    @TableField("plate_no")
    private String plateNo;
    /**
     * 挂车号
     */
    @TableField("trailer_no")
    private String trailerNo;
    /**
     * 商品车数量
     */
    @TableField("total_ship_count")
    private Integer totalShipCount;
    /**
     * 要求入库时间
     */
    @TableField("expect_Inbound_date")
    private Date expectInboundDate;
    /**
     * 要求回单时间
     */
    @TableField("expect_receipt_date")
    private Date expectReceiptDate;
    /**
     * 要求发运时间
     */
    @TableField("expcet_ship_date")
    private Date expcetShipDate;
    /**
     * 要求抵达时间
     */
    @TableField("expect_arrive_date")
    private Date expectArriveDate;
    /**
     * 日志头ID
     */
    @TableField("log_header_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long logHeaderId;
    /**
     * 来源唯一键
     */
    @TableField("source_key")
    private String sourceKey;
    /**
     * 来源单据号
     */
    @TableField("source_no")
    private String sourceNo;
    /**
     * 起运地编码
     */
    @TableField("origin_location_gid")
    private String originLocationGid;
    /**
     * 起运地名称
     */
    @TableField("origin_location_name")
    private String originLocationName;
    /**
     * 起运地省份
     */
    @TableField("origin_location_province")
    private String originLocationProvince;
    /**
     * 起运地城市
     */
    @TableField("origin_location_city")
    private String originLocationCity;
    /**
     * 起运地区县
     */
    @TableField("origin_location_county")
    private String originLocationCounty;
    /**
     * 起运地地址
     */
    @TableField("origin_location_address")
    private String originLocationAddress;
    /**
     * 目的地编码
     */
    @TableField("dest_location_id")
    private String destLocationId;
    /**
     * 目的地名称
     */
    @TableField("dest_location_name")
    private String destLocationName;
    /**
     * 目的地省份
     */
    @TableField("dest_location_province")
    private String destLocationProvince;
    /**
     * 目的地城市
     */
    @TableField("dest_location_city")
    private String destLocationCity;
    /**
     * 目的地区县
     */
    @TableField("dest_location_county")
    private String destLocationCounty;
    /**
     * 目的地地址
     */
    @TableField("dest_location_address")
    private String destLocationAddress;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 状态(10:正常,50:取消)
     */
    private String status;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    /**
     * 出入库类型(10:入库,20:出库,30:移库,40:其他)
     */
    @TableField("bound_type")
    private String boundType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShipmentGid() {
        return shipmentGid;
    }

    public void setShipmentGid(String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTransportModeGid() {
        return transportModeGid;
    }

    public void setTransportModeGid(String transportModeGid) {
        this.transportModeGid = transportModeGid;
    }

    public String getServiceProviderGid() {
        return serviceProviderGid;
    }

    public void setServiceProviderGid(String serviceProviderGid) {
        this.serviceProviderGid = serviceProviderGid;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getDriverGid() {
        return driverGid;
    }

    public void setDriverGid(String driverGid) {
        this.driverGid = driverGid;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public Integer getTotalShipCount() {
        return totalShipCount;
    }

    public void setTotalShipCount(Integer totalShipCount) {
        this.totalShipCount = totalShipCount;
    }

    public Date getExpectInboundDate() {
        return expectInboundDate;
    }

    public void setExpectInboundDate(Date expectInboundDate) {
        this.expectInboundDate = expectInboundDate;
    }

    public Date getExpectReceiptDate() {
        return expectReceiptDate;
    }

    public void setExpectReceiptDate(Date expectReceiptDate) {
        this.expectReceiptDate = expectReceiptDate;
    }

    public Date getExpcetShipDate() {
        return expcetShipDate;
    }

    public void setExpcetShipDate(Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate() {
        return expectArriveDate;
    }

    public void setExpectArriveDate(Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public Long getLogHeaderId() {
        return logHeaderId;
    }

    public void setLogHeaderId(Long logHeaderId) {
        this.logHeaderId = logHeaderId;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getOriginLocationGid() {
        return originLocationGid;
    }

    public void setOriginLocationGid(String originLocationGid) {
        this.originLocationGid = originLocationGid;
    }

    public String getOriginLocationName() {
        return originLocationName;
    }

    public void setOriginLocationName(String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getOriginLocationProvince() {
        return originLocationProvince;
    }

    public void setOriginLocationProvince(String originLocationProvince) {
        this.originLocationProvince = originLocationProvince;
    }

    public String getOriginLocationCity() {
        return originLocationCity;
    }

    public void setOriginLocationCity(String originLocationCity) {
        this.originLocationCity = originLocationCity;
    }

    public String getOriginLocationCounty() {
        return originLocationCounty;
    }

    public void setOriginLocationCounty(String originLocationCounty) {
        this.originLocationCounty = originLocationCounty;
    }

    public String getOriginLocationAddress() {
        return originLocationAddress;
    }

    public void setOriginLocationAddress(String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getDestLocationId() {
        return destLocationId;
    }

    public void setDestLocationId(String destLocationId) {
        this.destLocationId = destLocationId;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationAddress() {
        return destLocationAddress;
    }

    public void setDestLocationAddress(String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getBoundType() {
        return boundType;
    }

    public void setBoundType(String boundType) {
        this.boundType = boundType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OtmShipment{" +
                ", id=" + id +
                ", shipmentGid=" + shipmentGid +
                ", shipmentType=" + shipmentType +
                ", transactionCode=" + transactionCode +
                ", transportModeGid=" + transportModeGid +
                ", serviceProviderGid=" + serviceProviderGid +
                ", serviceProviderName=" + serviceProviderName +
                ", driverGid=" + driverGid +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", plateNo=" + plateNo +
                ", trailerNo=" + trailerNo +
                ", totalShipCount=" + totalShipCount +
                ", expectInboundDate=" + expectInboundDate +
                ", expectReceiptDate=" + expectReceiptDate +
                ", expcetShipDate=" + expcetShipDate +
                ", expectArriveDate=" + expectArriveDate +
                ", logHeaderId=" + logHeaderId +
                ", sourceKey=" + sourceKey +
                ", sourceNo=" + sourceNo +
                ", originLocationGid=" + originLocationGid +
                ", originLocationName=" + originLocationName +
                ", originLocationProvince=" + originLocationProvince +
                ", originLocationCity=" + originLocationCity +
                ", originLocationCounty=" + originLocationCounty +
                ", originLocationAddress=" + originLocationAddress +
                ", destLocationId=" + destLocationId +
                ", destLocationName=" + destLocationName +
                ", destLocationProvince=" + destLocationProvince +
                ", destLocationCity=" + destLocationCity +
                ", destLocationCounty=" + destLocationCounty +
                ", destLocationAddress=" + destLocationAddress +
                ", remarks=" + remarks +
                ", status=" + status +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", boundType=" + boundType +
                "}";
    }

    public List<OtmOrderRelease> getOtmOrderReleaseList() {
        return otmOrderReleaseList;
    }

    public void setOtmOrderReleaseList(List<OtmOrderRelease> otmOrderReleaseList) {
        this.otmOrderReleaseList = otmOrderReleaseList;
    }

    public void addOtmOrderRelease(OtmOrderRelease otmOrderRelease) {
        if (Objects.isNull(otmOrderReleaseList)) {
            otmOrderReleaseList = new ArrayList<>();
        }
        otmOrderReleaseList.add(otmOrderRelease);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtmShipment)) return false;
        OtmShipment that = (OtmShipment) o;
        return Objects.equals(shipmentGid, that.shipmentGid) &&
                Objects.equals(shipmentType, that.shipmentType) &&
                Objects.equals(transactionCode, that.transactionCode) &&
                Objects.equals(transportModeGid, that.transportModeGid) &&
                Objects.equals(serviceProviderGid, that.serviceProviderGid) &&
                Objects.equals(serviceProviderName, that.serviceProviderName) &&
                Objects.equals(driverGid, that.driverGid) &&
                Objects.equals(driverName, that.driverName) &&
                Objects.equals(driverPhone, that.driverPhone) &&
                Objects.equals(plateNo, that.plateNo) &&
                Objects.equals(trailerNo, that.trailerNo) &&
                Objects.equals(expectInboundDate, that.expectInboundDate) &&
                Objects.equals(expectReceiptDate, that.expectReceiptDate) &&
                Objects.equals(expcetShipDate, that.expcetShipDate) &&
                Objects.equals(expectArriveDate, that.expectArriveDate) &&
                Objects.equals(originLocationGid, that.originLocationGid) &&
                Objects.equals(originLocationName, that.originLocationName) &&
                Objects.equals(originLocationProvince, that.originLocationProvince) &&
                Objects.equals(originLocationCity, that.originLocationCity) &&
                Objects.equals(originLocationCounty, that.originLocationCounty) &&
                Objects.equals(originLocationAddress, that.originLocationAddress) &&
                Objects.equals(destLocationId, that.destLocationId) &&
                Objects.equals(destLocationName, that.destLocationName) &&
                Objects.equals(destLocationProvince, that.destLocationProvince) &&
                Objects.equals(destLocationCity, that.destLocationCity) &&
                Objects.equals(destLocationCounty, that.destLocationCounty) &&
                Objects.equals(destLocationAddress, that.destLocationAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shipmentGid, shipmentType, transactionCode, transportModeGid, serviceProviderGid, serviceProviderName, driverGid, driverName, driverPhone, plateNo, trailerNo, expectInboundDate, expectReceiptDate, expcetShipDate, expectArriveDate, originLocationGid, originLocationName, originLocationProvince, originLocationCity, originLocationCounty, originLocationAddress, destLocationId, destLocationName, destLocationProvince, destLocationCity, destLocationCounty, destLocationAddress);
    }
}
