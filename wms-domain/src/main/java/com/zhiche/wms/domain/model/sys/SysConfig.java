package com.zhiche.wms.domain.model.sys;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: WMS各项功能节点配置
 * @Date: Create in 16:16 2018/12/12
 */
@TableName("w_sys_config")
public class SysConfig extends Model<SysConfig> {
    @Override
    protected Serializable pkVal () {
        return null;
    }

    //主键id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    //	配置编码
    @TableField("code")
    private String code;


    //	配置名称
    @TableField("label")
    private String label;

    //	配置项说明
    @TableField("comment")
    private String comment;

    //	关联类型的唯一标识
    @TableField("releation_id")
    private String releationId;

    //	配置类型ROLE:基于角色,POINT:基于发车点,WAREHOUSE 基于仓库配置...
    @TableField("config_type")
    private String configType;

    //	配置级别 0必须不 1无影响 2必须
    @TableField("config_level")
    private String configLevel;

    //	= 等于,> 大于,<小于,≠ 不等于,≥ 大于等于,≤小于等于
    @TableField("config_condition")
    private String configCondition;

    //	配置数据
    @TableField("config_detail")
    private String configDetail;

    //系统创建时间
    @TableField("gmt_create")
    private Date gmtCreate;

    //更新时间
    @TableField("gmt_update")
    private Date gmtUpdate;

    //	创建人-名字
    @TableField("create_user")
    private String createUser;

    //	配置状态--0可用 1已删除 2禁用
    @TableField("config_status")
    private String configStatus;

    //	配置是否勾选 1 已选择配置,0 未配置
    @TableField("configed")
    private String configed;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getCode () {
        return code;
    }

    public void setCode (String code) {
        this.code = code;
    }

    public String getLabel () {
        return label;
    }

    public void setLabel (String label) {
        this.label = label;
    }

    public String getComment () {
        return comment;
    }

    public void setComment (String comment) {
        this.comment = comment;
    }

    public String getReleationId () {
        return releationId;
    }

    public void setReleationId (String releationId) {
        this.releationId = releationId;
    }

    public String getConfigType () {
        return configType;
    }

    public void setConfigType (String configType) {
        this.configType = configType;
    }

    public String getConfigLevel () {
        return configLevel;
    }

    public void setConfigLevel (String configLevel) {
        this.configLevel = configLevel;
    }

    public String getConfigCondition () {
        return configCondition;
    }

    public void setConfigCondition (String configCondition) {
        this.configCondition = configCondition;
    }

    public String getConfigDetail () {
        return configDetail;
    }

    public void setConfigDetail (String configDetail) {
        this.configDetail = configDetail;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate () {
        return gmtUpdate;
    }

    public void setGmtUpdate (Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getCreateUser () {
        return createUser;
    }

    public void setCreateUser (String createUser) {
        this.createUser = createUser;
    }

    public String getConfigStatus () {
        return configStatus;
    }

    public void setConfigStatus (String configStatus) {
        this.configStatus = configStatus;
    }

    public String getConfiged () {
        return configed;
    }

    public void setConfiged (String configed) {
        this.configed = configed;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("SysConfig{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", label='").append(label).append('\'');
        sb.append(", comment='").append(comment).append('\'');
        sb.append(", releationId='").append(releationId).append('\'');
        sb.append(", configType='").append(configType).append('\'');
        sb.append(", configLevel='").append(configLevel).append('\'');
        sb.append(", configCondition='").append(configCondition).append('\'');
        sb.append(", configDetail='").append(configDetail).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append(", createUser='").append(createUser).append('\'');
        sb.append(", configStatus='").append(configStatus).append('\'');
        sb.append(", configed='").append(configed).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
