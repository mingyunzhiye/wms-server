package com.zhiche.wms.domain.model.opbaas;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 状态日志
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-11
 */
@TableName("status_log")
public class StatusLog extends Model<StatusLog> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 业务表(10:otm_order_release,20:otm_order_release)
     */
	@TableField("table_type")
	private String tableType;
    /**
     * 业务表ID
     */
	@TableField("table_id")
	private String tableId;
    /**
     * 状态值
     */
	private String status;
    /**
     * 状态名称
     */
	@TableField("status_name")
	private String statusName;
    /**
     * 创建人
     */
	@TableField("user_create")
	private String userCreate;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(String userCreate) {
		this.userCreate = userCreate;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "StatusLog{" +
			", id=" + id +
			", tableType=" + tableType +
			", tableId=" + tableId +
			", status=" + status +
			", statusName=" + statusName +
			", userCreate=" + userCreate +
			", gmtCreate=" + gmtCreate +
			"}";
	}
}
