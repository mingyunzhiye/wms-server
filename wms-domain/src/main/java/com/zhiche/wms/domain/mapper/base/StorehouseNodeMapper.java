package com.zhiche.wms.domain.mapper.base;

import com.zhiche.wms.domain.model.base.StorehouseNode;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 仓库节点配置 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
public interface StorehouseNodeMapper extends BaseMapper<StorehouseNode> {

}
