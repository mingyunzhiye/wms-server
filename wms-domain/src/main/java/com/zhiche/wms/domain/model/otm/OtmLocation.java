package com.zhiche.wms.domain.model.otm;

/**
 * Created by zhaoguixin on 2018/6/15.
 */
public class OtmLocation {

    /**
     * 地址编码
     */
    private String locationGid;
    /**
     * 地址名称
     */
    private String locationName;
    /**
     * 地址描述
     */
    private String description;
    /**
     * 省
     */
    private String zone1;
    /**
     * 市
     */
    private String zone2;
    /**
     * 县
     */
    private String zone3;

    public String getLocationGid() {
        return locationGid;
    }

    public void setLocationGid(String locationGid) {
        this.locationGid = locationGid;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getZone1() {
        return zone1;
    }

    public void setZone1(String zone1) {
        this.zone1 = zone1;
    }

    public String getZone2() {
        return zone2;
    }

    public void setZone2(String zone2) {
        this.zone2 = zone2;
    }

    public String getZone3() {
        return zone3;
    }

    public void setZone3(String zone3) {
        this.zone3 = zone3;
    }

    @Override
    public String toString() {
        return "OtmLocationDTO{" +
                "locationGid='" + locationGid + '\'' +
                ", locationName='" + locationName + '\'' +
                ", description='" + description + '\'' +
                ", zone1='" + zone1 + '\'' +
                ", zone2='" + zone2 + '\'' +
                ", zone3='" + zone3 + '\'' +
                '}';
    }
}
