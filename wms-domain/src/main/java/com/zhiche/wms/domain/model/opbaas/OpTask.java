package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 作业任务
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@TableName("op_task")
public class OpTask extends Model<OpTask> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
    /**
     * 指令号
     */
    @TableField("dispatch_no")
    private String dispatchNo;
    /**
     * 运单号
     */
    @TableField("waybill_no")
    private String waybillNo;
    /**
     * 任务类型(10：寻车，20：移车，30：提车)
     */
    private String type;
    /**
     * 任务状态(10：创建，20：开始，30：完成，40：取消)
     */
    private String status;
    /**
     * 司机编号
     */
    @TableField("driver_code")
    private String driverCode;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机电话
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 任务开始时间
     */
    @TableField("start_time")
    private Date startTime;
    /**
     * 任务完成时间
     */
    @TableField("finish_time")
    private Date finishTime;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDispatchNo() {
        return dispatchNo;
    }

    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDriverCode() {
        return driverCode;
    }

    public void setDriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OpTask{" +
                ", id=" + id +
                ", dispatchNo=" + dispatchNo +
                ", waybillNo=" + waybillNo +
                ", type=" + type +
                ", status=" + status +
                ", driverCode=" + driverCode +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
