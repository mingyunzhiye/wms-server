package com.zhiche.wms.domain.mapper.otm;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.dto.opbaas.paramdto.OrderReleaseParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ReleaseWithShipmentDTO;
import com.zhiche.wms.dto.opbaas.resultdto.NodeProcessInfoDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskReleaseResultDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 运单 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public interface OtmOrderReleaseMapper extends BaseMapper<OtmOrderRelease> {

    List<OrderReleaseParamDTO> queryOrderReleaseList(Page<OrderReleaseParamDTO> page, @Param("ew") Wrapper<OrderReleaseParamDTO> ew);

    List<TaskReleaseResultDTO> selectReleaseInfo(HashMap<String, Object> params);

    NodeProcessInfoDTO queryNodeInfo(@Param("ew") Wrapper<NodeProcessInfoDTO> ew);

    List<ReleaseWithShipmentDTO> queryReleaseShipList(Page<ReleaseWithShipmentDTO> page, @Param("ew") EntityWrapper<ReleaseWithShipmentDTO> ew);

    List<ReleaseWithShipmentDTO> getReleaseShipDetail(@Param("ew") EntityWrapper<ReleaseWithShipmentDTO> ew);

    void updateSQLMode();

}
