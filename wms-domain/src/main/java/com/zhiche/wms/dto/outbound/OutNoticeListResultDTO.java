package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

public class OutNoticeListResultDTO implements Serializable {
    @JsonSerialize(using=ToStringSerializer.class)
    private Long noticeLineId;
    @JsonSerialize(using=ToStringSerializer.class)
    private Long noticeHeadId;
    private String noticeNo;
    private String ownerId;
    private String ownerOrderNo;
    private String lotNo1;
    private String carrierName;
    private String materielId;
    private String materielCode;
    private String materielName;
    private String driverName;
    private String plateNumber;
    private String driverPhone;
    private String noticeLineStatus;
    private String prepareLineStatus;
    private String preparetor;
    private String storeHouseName;
    private Date expectDate;
    private Date createDate;
    private String inboundStatus;
    private String inspectStatus;
    private String remarks;
    private Date startTime;
    private Date finishTime;

    public Long getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(Long noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public Long getNoticeHeadId() {
        return noticeHeadId;
    }

    public void setNoticeHeadId(Long noticeHeadId) {
        this.noticeHeadId = noticeHeadId;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getNoticeLineStatus() {
        return noticeLineStatus;
    }

    public void setNoticeLineStatus(String noticeLineStatus) {
        this.noticeLineStatus = noticeLineStatus;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getPreparetor() {
        return preparetor;
    }

    public void setPreparetor(String preparetor) {
        this.preparetor = preparetor;
    }

    public String getPrepareLineStatus() {
        return prepareLineStatus;
    }

    public void setPrepareLineStatus(String prepareLineStatus) {
        this.prepareLineStatus = prepareLineStatus;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getStoreHouseName() {
        return storeHouseName;
    }

    public void setStoreHouseName(String storeHouseName) {
        this.storeHouseName = storeHouseName;
    }

    public Date getExpectDate() {
        return expectDate;
    }

    public void setExpectDate(Date expectDate) {
        this.expectDate = expectDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getInboundStatus() {
        return inboundStatus;
    }

    public void setInboundStatus(String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    public String getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(String inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
