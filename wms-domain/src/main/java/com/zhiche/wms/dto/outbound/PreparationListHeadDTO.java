package com.zhiche.wms.dto.outbound;

import java.io.Serializable;
import java.util.Date;

public class PreparationListHeadDTO implements Serializable {
    private String noticeHeadId;
    private String noticeHeadStatus;
    private String noticeNo;
    private String ownerId;
    private String userCreate;
    private String plateNumber;
    private Date gmtCreate;
    private Date formatCreate;
    private String prepareStatus;
    private String preparationNo;
    private Date planTime;
    private String noticeHeadSourceNo;
    private String loadingArea;
    private String noticeHeadSourceKey;
    private String houseId;
    private String houseName;
    private String ownerOrderNo;
    private String noticeLineId;
    private String lotNo1;
    private String providerName;
    private String finishTime;
    private String headId;

    public String getHeadId() {
        return headId;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public Date getFormatCreate() {
        return formatCreate;
    }

    public void setFormatCreate(Date formatCreate) {
        this.formatCreate = formatCreate;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(String noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getNoticeHeadSourceKey() {
        return noticeHeadSourceKey;
    }

    public void setNoticeHeadSourceKey(String noticeHeadSourceKey) {
        this.noticeHeadSourceKey = noticeHeadSourceKey;
    }

    public String getLoadingArea() {
        return loadingArea;
    }

    public void setLoadingArea(String loadingArea) {
        this.loadingArea = loadingArea;
    }

    public String getNoticeHeadSourceNo() {
        return noticeHeadSourceNo;
    }

    public void setNoticeHeadSourceNo(String noticeHeadSourceNo) {
        this.noticeHeadSourceNo = noticeHeadSourceNo;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getNoticeHeadId() {
        return noticeHeadId;
    }

    public void setNoticeHeadId(String noticeHeadId) {
        this.noticeHeadId = noticeHeadId;
    }

    public String getNoticeHeadStatus() {
        return noticeHeadStatus;
    }

    public void setNoticeHeadStatus(String noticeHeadStatus) {
        this.noticeHeadStatus = noticeHeadStatus;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }

    public String getPrepareStatus() {
        return prepareStatus;
    }

    public void setPrepareStatus(String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public String getPreparationNo() {
        return preparationNo;
    }

    public void setPreparationNo(String preparationNo) {
        this.preparationNo = preparationNo;
    }
}
