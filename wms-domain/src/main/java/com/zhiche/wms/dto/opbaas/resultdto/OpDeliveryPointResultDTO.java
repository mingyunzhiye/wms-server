package com.zhiche.wms.dto.opbaas.resultdto;

import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;

import java.io.Serializable;

public class OpDeliveryPointResultDTO extends OpDeliveryPoint implements Serializable {
    private String[] nodeOption;

    public String[] getNodeOption() {
        return nodeOption;
    }

    public void setNodeOption(String[] nodeOption) {
        this.nodeOption = nodeOption;
    }
}
