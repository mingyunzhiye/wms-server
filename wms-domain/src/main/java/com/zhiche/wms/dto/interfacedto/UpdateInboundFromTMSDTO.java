package com.zhiche.wms.dto.interfacedto;


import java.io.Serializable;

/**
 * wms供tms接口的参数传输对象
 */
public class UpdateInboundFromTMSDTO implements Serializable {
    private String hKey; //校验接口调用的key
    private String updateOrderDto;//传入的参数 字符串格式


    public void sethKey(String hKey) {
        this.hKey = hKey;
    }

    public String gethKey() {
        return hKey;
    }

    public String getUpdateOrderDto() {
        return updateOrderDto;
    }

    public void setUpdateOrderDto(String updateOrderDto) {
        this.updateOrderDto = updateOrderDto;
    }
}
