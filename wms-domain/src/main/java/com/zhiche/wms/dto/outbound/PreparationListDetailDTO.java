package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PreparationListDetailDTO implements Serializable {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lineId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headerId;
    private String seq;
    private String lineSourceKey;
    private String lineSourceNo;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logLineId;
    private String materielId;
    private String materielCode;
    private String materielName;
    private String uom;
    private BigDecimal expectQty;
    private BigDecimal expectNetWeight;
    private BigDecimal expectGrossWeight;
    private BigDecimal expectGrossCubage;
    private BigDecimal expectPackedCount;
    private BigDecimal outboundQty;
    private BigDecimal outboundNetWeight;
    private BigDecimal outboundGrossWeight;
    private BigDecimal outboundGrossCubage;
    private BigDecimal outboundPackedCount;
    private String lotNo0;
    private String lotNo1;
    private String lotNo2;
    private String lotNo3;
    private String lotNo4;
    private String lotNo5;
    private String lotNo6;
    private String lotNo7;
    private String lotNo8;
    private String lotNo9;
    private String lineStatus;//状态(10:未入库,20:部分入库,30:全部入库,40:关闭入库,40:取消)
    private String lineRemarks;
    private Date lineCreate;
    private Date lineModified;
    private Long headId;
    private Long storeHouseId;
    private String noticeNo;
    private String ownerId;
    private String ownerOrderNo;
    private Date orderDate;
    private String carrierId;
    private String carrierName;
    private String transportMethod;
    private String plateNumber;
    private String driverName;
    private String driverPhone;
    private Date expectShipDateLower;
    private Date expectShipDateUpper;
    private String genMethod;//(10:手工创建,20:oms系统触发,30:tms系统触发,40:君马系统触发)
    private String headStatus;//(10:未出库,20:部分出库,30:全部出库,40:关闭出库,50:取消)
    private Long logHeaderId;
    private String headSourceKey;
    private String headSourceNo;
    private String remarks;
    private String userCreate;
    private String userModified;
    private String preparationStatus;
    private String preparetor;
    private Date planTime;
    private Date startTime;
    private Date finishTime;
    private String locationNo;
    private String locationId;
    private String destAddress;
    private String prepareLineStatus;
    private String cusWaybillNo;
    private String cusOrderNo;
    private String dealerName;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long prepareLineId;

    public Long getPrepareLineId() {
        return prepareLineId;
    }

    public void setPrepareLineId(Long prepareLineId) {
        this.prepareLineId = prepareLineId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getCusOrderNo() {
        return cusOrderNo;
    }

    public void setCusOrderNo(String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybillNo() {
        return cusWaybillNo;
    }

    public void setCusWaybillNo(String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getPrepareLineStatus() {
        return prepareLineStatus;
    }

    public void setPrepareLineStatus(String prepareLineStatus) {
        this.prepareLineStatus = prepareLineStatus;
    }

    public String getDestAddress() {
        return destAddress;
    }

    public void setDestAddress(String destAddress) {
        this.destAddress = destAddress;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getPreparetor() {
        return preparetor;
    }

    public void setPreparetor(String preparetor) {
        this.preparetor = preparetor;
    }

    public Long getLineId() {
        return lineId;
    }

    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getLineSourceKey() {
        return lineSourceKey;
    }

    public void setLineSourceKey(String lineSourceKey) {
        this.lineSourceKey = lineSourceKey;
    }

    public String getLineSourceNo() {
        return lineSourceNo;
    }

    public void setLineSourceNo(String lineSourceNo) {
        this.lineSourceNo = lineSourceNo;
    }

    public Long getLogLineId() {
        return logLineId;
    }

    public void setLogLineId(Long logLineId) {
        this.logLineId = logLineId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectQty() {
        return expectQty;
    }

    public void setExpectQty(BigDecimal expectQty) {
        this.expectQty = expectQty;
    }

    public BigDecimal getExpectNetWeight() {
        return expectNetWeight;
    }

    public void setExpectNetWeight(BigDecimal expectNetWeight) {
        this.expectNetWeight = expectNetWeight;
    }

    public BigDecimal getExpectGrossWeight() {
        return expectGrossWeight;
    }

    public void setExpectGrossWeight(BigDecimal expectGrossWeight) {
        this.expectGrossWeight = expectGrossWeight;
    }

    public BigDecimal getExpectGrossCubage() {
        return expectGrossCubage;
    }

    public void setExpectGrossCubage(BigDecimal expectGrossCubage) {
        this.expectGrossCubage = expectGrossCubage;
    }

    public BigDecimal getExpectPackedCount() {
        return expectPackedCount;
    }

    public void setExpectPackedCount(BigDecimal expectPackedCount) {
        this.expectPackedCount = expectPackedCount;
    }

    public BigDecimal getOutboundQty() {
        return outboundQty;
    }

    public void setOutboundQty(BigDecimal outboundQty) {
        this.outboundQty = outboundQty;
    }

    public BigDecimal getOutboundNetWeight() {
        return outboundNetWeight;
    }

    public void setOutboundNetWeight(BigDecimal outboundNetWeight) {
        this.outboundNetWeight = outboundNetWeight;
    }

    public BigDecimal getOutboundGrossWeight() {
        return outboundGrossWeight;
    }

    public void setOutboundGrossWeight(BigDecimal outboundGrossWeight) {
        this.outboundGrossWeight = outboundGrossWeight;
    }

    public BigDecimal getOutboundGrossCubage() {
        return outboundGrossCubage;
    }

    public void setOutboundGrossCubage(BigDecimal outboundGrossCubage) {
        this.outboundGrossCubage = outboundGrossCubage;
    }

    public BigDecimal getOutboundPackedCount() {
        return outboundPackedCount;
    }

    public void setOutboundPackedCount(BigDecimal outboundPackedCount) {
        this.outboundPackedCount = outboundPackedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getLineStatus() {
        return lineStatus;
    }

    public void setLineStatus(String lineStatus) {
        this.lineStatus = lineStatus;
    }

    public String getLineRemarks() {
        return lineRemarks;
    }

    public void setLineRemarks(String lineRemarks) {
        this.lineRemarks = lineRemarks;
    }

    public Date getLineCreate() {
        return lineCreate;
    }

    public void setLineCreate(Date lineCreate) {
        this.lineCreate = lineCreate;
    }

    public Date getLineModified() {
        return lineModified;
    }

    public void setLineModified(Date lineModified) {
        this.lineModified = lineModified;
    }

    public Long getHeadId() {
        return headId;
    }

    public void setHeadId(Long headId) {
        this.headId = headId;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getExpectShipDateLower() {
        return expectShipDateLower;
    }

    public void setExpectShipDateLower(Date expectShipDateLower) {
        this.expectShipDateLower = expectShipDateLower;
    }

    public Date getExpectShipDateUpper() {
        return expectShipDateUpper;
    }

    public void setExpectShipDateUpper(Date expectShipDateUpper) {
        this.expectShipDateUpper = expectShipDateUpper;
    }

    public String getGenMethod() {
        return genMethod;
    }

    public void setGenMethod(String genMethod) {
        this.genMethod = genMethod;
    }

    public String getHeadStatus() {
        return headStatus;
    }

    public void setHeadStatus(String headStatus) {
        this.headStatus = headStatus;
    }

    public Long getLogHeaderId() {
        return logHeaderId;
    }

    public void setLogHeaderId(Long logHeaderId) {
        this.logHeaderId = logHeaderId;
    }

    public String getHeadSourceNo() {
        return headSourceNo;
    }

    public void setHeadSourceNo(String headSourceNo) {
        this.headSourceNo = headSourceNo;
    }

    public String getHeadSourceKey() {
        return headSourceKey;
    }

    public void setHeadSourceKey(String headSourceKey) {
        this.headSourceKey = headSourceKey;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public String getPreparationStatus() {
        return preparationStatus;
    }

    public void setPreparationStatus(String preparationStatus) {
        this.preparationStatus = preparationStatus;
    }

}
