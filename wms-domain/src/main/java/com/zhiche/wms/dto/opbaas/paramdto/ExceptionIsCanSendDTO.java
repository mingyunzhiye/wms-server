package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;
import java.util.Date;

public class ExceptionIsCanSendDTO implements Serializable{
    /**
     * VIN码
     */
    private String vin_code;
    /**
     * VIN码
     */
    private String register_time;
    /**
     * Y/N
     */
    private String is_send;


    public String getVin_code() {
        return vin_code;
    }

    public void setVin_code(String vin_code) {
        this.vin_code = vin_code;
    }

    public String getRegister_time() {
        return register_time;
    }

    public void setRegister_time(String register_time) {
        this.register_time = register_time;
    }

    public String getIs_send() {
        return is_send;
    }

    public void setIs_send(String is_send) {
        this.is_send = is_send;
    }
}
