package com.zhiche.wms.app.controller.stock;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.dto.opbaas.paramdto.AppCommonQueryDTO;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.stock.StockDTO;
import com.zhiche.wms.service.stock.IStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by hxh on 2018/8/2.
 */
@Api(value = "stock-API", description = "库位调整操作接口")
@RestController
@RequestMapping(value = "/stock", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private IStockService stockService;

    @PostMapping(value = "/queryPage")
    @ResponseBody
    public RestfulResponse<Page<StockDTO>> queryPage(AppQueryVo page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<Page<StockDTO>> result = new RestfulResponse<>(0, "success");
        try {
            Page<StockDTO> stockDTOPage = new Page<StockDTO>();
            Map<String, Object> map = new HashMap<>();
            map.put("storeHouseId", page.getHouseId());
            map.put("lotNo1", page.getKey());
            stockDTOPage.setCondition(map);
            stockDTOPage.setSize(page.getSize());
            stockDTOPage.setCurrent(page.getCurrent());
            Page<StockDTO> resultPage = stockService.queryPageStock(stockDTOPage);
            result.setData(resultPage);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    @PostMapping(value = "/getStockInfo")
    @ResponseBody
    public RestfulResponse<StockDTO> getStockInfo(AppQueryVo page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<StockDTO> result = new RestfulResponse<>(0, "success");
        try {
            Page<StockDTO> stockDTOPage = new Page<>();
            Page<StockDTO> resultPage = null;
            if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(page.getVisitType())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeHouseId", page.getHouseId());
                map.put("stockId", page.getKey());
                stockDTOPage.setCondition(map);
                resultPage = stockService.queryPageStock(stockDTOPage);
            } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(page.getVisitType())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeHouseId", page.getHouseId());
                map.put("key", page.getKey());
                stockDTOPage.setCondition(map);
                resultPage = stockService.queryPageStockByQrCode(stockDTOPage);
            }
            if (resultPage != null
                    && Objects.nonNull(resultPage.getRecords())
                    && resultPage.getRecords().size() > 0) {
                result.setData(resultPage.getRecords().get(0));
            } else {
                result.setCode(-1);
                result.setMessage("无相关数据");
            }
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 更新库位
     */
    @PostMapping("/updateStockLocation")
    @ResponseBody
    public RestfulResponse<Object> updateStockLocation(CommonConditionParamDTO dto) {
        stockService.updateStockLocation(dto.getCondition());
        return new RestfulResponse<>(0, "调整成功");
    }

    /**
     * 目的仓库无人值守时自动入库
     * @param conditions {}
     * @return
     */
    @PostMapping(value = "/unattendedAutoWareHouse")
    @ApiOperation(value = "目的仓库无人值守时自动入库")
    public RestfulResponse unattendedAutoWareHouse (@RequestBody AppCommonQueryDTO conditions) {
        LOGGER.info("/unattendedAutoWareHouse （目的仓库无人值守时自动入库） params : {} " ,conditions);
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            if(null == conditions){
                throw new BaseException(99,"参数不能为空");
            }
            stockService.unattendedAutoWareHouse(conditions);
        }catch (BaseException be){
            LOGGER.error("/unattendedAutoWareHouse （目的仓库无人值守时自动入库）  BaseException ERROR... ", be);
            result = new RestfulResponse(be.getCode(), be.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("/unattendedAutoWareHouse （目的仓库无人值守时自动入库）  ERROR... ", e);
            result = new RestfulResponse(99, "failed", null);
        }
        return result;
    }

}
