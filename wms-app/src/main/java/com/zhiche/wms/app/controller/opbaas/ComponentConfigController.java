package com.zhiche.wms.app.controller.opbaas;


import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.opbaas.paramdto.MissingRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;
import com.zhiche.wms.service.opbaas.IComponentConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 缺件配置 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
@Api(value = "componentConfig-API", description = "缺件配置操作接口")
@RestController
@RequestMapping("/componentConfig")
public class ComponentConfigController {

    @Autowired
    IComponentConfigService componentConfigService;

    @ApiOperation(value = "获取配置信息", notes = "获取配置信息")
    @PostMapping("/getComponentInfo")
    @Deprecated
    public RestfulResponse<List<MissingRegidterDetailDTO>> getComponentInfo() {

        RestfulResponse<List<MissingRegidterDetailDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        List<MissingRegidterDetailDTO> resultDTOList = componentConfigService.queryComponent();
        restfulResponse.setData(resultDTOList);
        return restfulResponse;
    }

}

