package com.zhiche.wms.app.controller.outbound;


import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.InterfaceVisitTypeEnum;
import com.zhiche.wms.dto.outbound.OutboundPrepareDTO;
import com.zhiche.wms.service.outbound.IOutboundPrepareLineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 出库备料单明细 前端控制器
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
@Api(value = "outboundPrepareLine-API", description = "出库备料接口")
@Controller
@RequestMapping("/outboundPrepare")
public class OutboundPrepareLineController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    private IOutboundPrepareLineService iOutboundPrepareLineService;


    /**
     * 出库备料-模糊匹配
     */
    @PostMapping("/queryList")
    @ResponseBody
    @ApiOperation(value = "查询搜索未备料的出库通知", notes = "查询搜索未备料的出库通知", response = OutboundPrepareDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "模糊搜索条件"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "current", value = "页数"),
            @ApiImplicitParam(name = "size", value = "每页条数")
    })
    public RestfulResponse<List<OutboundPrepareDTO>> queryPageByLotId(AppQueryVo page) {
        RestfulResponse<List<OutboundPrepareDTO>> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            List<OutboundPrepareDTO> outboundPrepareDTOS = iOutboundPrepareLineService.queryPageByLotId(
                    page.getKey(),
                    page.getHouseId().toString(),
                    page.getCurrent(),
                    page.getSize());
            restfulResponse.setData(outboundPrepareDTOS);
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/getPrepare")
    @ResponseBody
    @ApiOperation(value = "查询搜索未备料的出库通知", notes = "查询搜索未备料的出库通知", response = OutboundPrepareDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "模糊搜索条件"),
            @ApiImplicitParam(name = "houseId", value = "仓库号"),
            @ApiImplicitParam(name = "visitType", value = "查询的方式（单击进入，扫码进入）")
    })
    public RestfulResponse<OutboundPrepareDTO> getResultByNoticeId(AppQueryVo page) {
        RestfulResponse<OutboundPrepareDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            if (InterfaceVisitTypeEnum.CLICK_TYPE.getCode().equals(page.getVisitType())) {
                //如果是点击
                OutboundPrepareDTO prepare = iOutboundPrepareLineService.getPrepare(page.getKey(), page.getHouseId());
//                inspect.setTaskType("40");
                restfulResponse.setData(prepare);
            } else if (InterfaceVisitTypeEnum.SCAN_TYPE.getCode().equals(page.getVisitType())) {
                //如果是扫码
                OutboundPrepareDTO prepare = iOutboundPrepareLineService.getPrepareBykey(page.getKey(), page.getHouseId());
//                inspect.setTaskType("40");
                restfulResponse.setData(prepare);
            }
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }

    @PostMapping("/prepareConfirm")
    @ResponseBody
    public RestfulResponse<OutboundPrepareDTO> Inspect(AppQueryVo page) {
        LOGGER.info("InboundInspectLineController:inboundInspect:page-->{}", page.toString());
        RestfulResponse<OutboundPrepareDTO> restfulResponse = new RestfulResponse<>(0, "success", null);
        try {
            OutboundPrepareDTO outboundPrepareDTO = iOutboundPrepareLineService.updateStatus(page.getKey(), page.getHouseId());
            restfulResponse.setData(outboundPrepareDTO);
            restfulResponse.setMessage("备料开始");
        } catch (BaseException e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Controller:\t" + e.toString());
            restfulResponse.setCode(-1);
            restfulResponse.setMessage("操作失败，请重试");
        }
        return restfulResponse;
    }
}

