package com.zhiche.wms.app.controller.opbaas;


import com.zhiche.wms.domain.model.opbaas.OpQrCodeBind;
import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.base.ResultDTOWithPagination;
import com.zhiche.wms.dto.opbaas.paramdto.TaskControllerParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskDetailResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.TaskResultDTO;
import com.zhiche.wms.service.opbaas.ITaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 作业任务 前端控制器
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Api(value = "task-API", description = "任务操作")
@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private ITaskService iTaskService;

    /**
     * <p>
     * 提车首页-根据订单号/运单号/车架号搜索任务
     * </p>
     */
    @ApiOperation(value = "搜索任务", notes = "据订单号/运单号/车架号,返回ResultDTO的json对象")
    @PostMapping("/getOrderInfo")
    public ResultDTO<List<TaskResultDTO>> getTaskInfo(TaskControllerParamDTO dto) {
        List<TaskResultDTO> resultDTOS = iTaskService.queryTaskInfo(dto);
        return new ResultDTO<>(true, resultDTOS, "查询操作成功");
    }

    /**
     * <p>
     * 提车首页-任务列表查询
     * </p>
     */
    @ApiOperation(value = "任务详细信息(扫描方式)", notes = "任务列表返回ResultDTO的json对象")
    @PostMapping("/getTaskDetailByQrCode")
    public ResultDTO<TaskDetailResultDTO> getTaskDetailByQrCode(TaskControllerParamDTO dto) {
        TaskDetailResultDTO detailByQrCode = iTaskService.getDetailByScan(dto);
        return new ResultDTO<>(true, detailByQrCode, "查询成功");
    }

    /**
     * <p>
     * 提车首页-任务列表查询
     * </p>
     */
    @ApiOperation(value = "任务列表", notes = "任务列表返回ResultDTO的json对象")
    @PostMapping("/getTaskList")
    public ResultDTOWithPagination<List<TaskResultDTO>> getTaskList(TaskControllerParamDTO dto) {
        ResultDTOWithPagination<List<TaskResultDTO>> pagination = iTaskService.queryTaskList(dto);
        pagination.setSuccess(true);
        pagination.setMessage("查询成功");
        return pagination;
    }

    ///**
    // * <p>
    // * 提车首页-任务列表查询
    // * </p>
    // */
    //@ApiOperation(value = "任务列表", notes = "任务列表返回ResultDTO的json对象")
    //@PostMapping("/getTaskList")
    //public RestfulResponse<Page<TaskResultDTO>> taskPage(TaskControllerParamDTO dto) {
    //    Page<TaskResultDTO> data = iTaskService.queryTaskPage(dto);
    //    return new RestfulResponse<>(0, "查询成功", data);
    //}


    /**
     * <p>
     * 提车首页-根据任务获取任务明细信息
     * </p>
     */
    @ApiOperation(value = "任务详细信息(点击任务方式)", notes = "任务列表返回ResultDTO的json对象")
    @PostMapping("/getTaskDetailByTask")
    public ResultDTO<TaskDetailResultDTO> getTaskDetailByTask(TaskControllerParamDTO dto) {
        TaskDetailResultDTO detailByTask = iTaskService.getTaskDetail(dto);
        return new ResultDTO<>(true, detailByTask, "查询成功");
    }


    /**
     * <p>
     * 移车任务-完成移车任务
     * </p>
     *
     * @param dto 参数封装
     */
    @ApiOperation(value = "完成移车", notes = "完成移车操作")
    @PostMapping("/finishForMove")
    public ResultDTO<OpQrCodeBind> updateFinishForMove(TaskControllerParamDTO dto) {
        ResultDTO<OpQrCodeBind> resultDTO = new ResultDTO<>(true, null, "操作成功!");
        iTaskService.updateMoveTask(dto);
        return resultDTO;
    }

    /**
     * <p>
     * 提车任务--开始提车
     * </p>
     */
    @ApiOperation(value = "开始提车", notes = "开始提车")
    @PostMapping("/updateStartTaskForPick")
    public ResultDTO<TaskDetailResultDTO> updateStartTaskForPick(TaskControllerParamDTO dto) {
        iTaskService.updatePickTask(dto);
        return new ResultDTO<>(true, null, "操作成功");
    }

    /**
     * <p>
     * 个人任务
     * </p>
     */
    @ApiOperation(value = "我的任务", notes = "我的任务按钮请求")
    @PostMapping("/myTask")
    public ResultDTOWithPagination<List<TaskResultDTO>> getMyTaskByType(TaskControllerParamDTO dto) {
        ResultDTOWithPagination<List<TaskResultDTO>> pagination = iTaskService.getMyTaskByType(dto);
        pagination.setSuccess(true);
        pagination.setMessage("查询成功!");
        return pagination;
    }

}

