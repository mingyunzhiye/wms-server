package com.zhiche.wms.app.controller.common;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExceptionRegisterDetailDTO;
import com.zhiche.wms.service.common.CommonExceptionService;
import com.zhiche.wms.service.opbaas.IExceptionConfigurationService;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "commonException-API", description = "异常登记通用接口")
@RestController
@RequestMapping("/commonException")
public class CommonExceptionController {

    @Autowired
    private CommonExceptionService commonExceptionService;
    @Autowired
    private IExceptionConfigurationService configurationService;
    @Autowired
    private IExceptionRegisterService registerService;

    /**
     * 获取九宫格异常信息
     */
    @ApiOperation(value = "getExceptionInfo", notes = "获取区域九宫格异常信息")
    @PostMapping("/getExceptionInfo")
    public RestfulResponse<List<ExceptionRegisterDetailDTO>> getExceptionInfo(ExceptionRegisterParamDTO dto) {
        List<ExceptionRegisterDetailDTO> exceptionInfo = commonExceptionService.getExceptionInfo(dto);
        return new RestfulResponse<>(0, "查询成功", exceptionInfo);
    }

    /**
     * 获取所有异常信息
     */
    @ApiOperation(value = "getAllExceptionPicUrl", notes = "获取所有异常信息")
    @PostMapping("/getAllExceptionPicUrl")
    public RestfulResponse<Page<ExResultDTO>> getAllExceptionPicUrl(ExceptionRegisterParamDTO dto) {
        RestfulResponse<Page<ExResultDTO>> restfulResponse = new RestfulResponse<>(0, "success");
        Page<ExResultDTO> resultDTOPage = commonExceptionService.getAllExceptionPicUrl(dto);
        restfulResponse.setData(resultDTOPage);
        return restfulResponse;
    }

    /**
     * 获取异常信息总数
     */
    @ApiOperation(value = "getExceptionTotal", notes = "获取当前对象异常总数")
    @PostMapping("/getExceptionTotal")
    public RestfulResponse<Integer> getExceptionTotal(ExceptionRegisterParamDTO dto) {
        RestfulResponse<Integer> restfulResponse = new RestfulResponse<>(0, "success");
        int total = commonExceptionService.getExceptionTotal(dto);
        restfulResponse.setData(total);
        return restfulResponse;
    }

    /**
     * 获取菜单信息
     */
    @ApiOperation(value = "getExceptionConfiguration", notes = "获取异常菜单信息")
    @PostMapping("/getExceptionConfiguration")
    public RestfulResponse<List<ExConfigurationDTO>> getExceptionConfiguration(ExceptionRegisterParamDTO dto) {
        List<ExConfigurationDTO> configurationList = configurationService.getConfigurationList(dto);
        return new RestfulResponse<>(0, "查询成功", configurationList);
    }

    /**
     * 获取图片信息
     */
    @ApiOperation(value = "getExceptionPicUrl", notes = "获取异常图片信息")
    @RequestMapping("/getExceptionPicUrl")
    public RestfulResponse<List<ExResultDTO>> getPictureUrl(ExceptionRegisterParamDTO dto) {
        List<ExResultDTO> exceptionPicUrl = registerService.getExceptionPicUrl(dto);
        return new RestfulResponse<>(0, "查询成功", exceptionPicUrl);
    }


    /**
     * 标记异常信息
     */
    @ApiOperation(value = "noteException", notes = "标记九宫格异常信息")
    @PostMapping("/noteException")
    public RestfulResponse<Object> updateNoteException(ExceptionRegisterParamDTO dto) {
        registerService.updateNoteException(dto);
        return new RestfulResponse<>(0, "标记成功", null);
    }
}
