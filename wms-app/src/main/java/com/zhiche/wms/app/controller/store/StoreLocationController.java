package com.zhiche.wms.app.controller.store;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.app.vo.AppQueryVo;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.service.base.IStoreLocationService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hxh on 2018/8/3.
 */
@Api(value = "storeLocation-API", description = "库位相关接口")
@RestController
@RequestMapping(value = "/storeLocation", produces = MediaType.APPLICATION_JSON_VALUE)
public class StoreLocationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoreLocationController.class);

    @Autowired
    private IStoreLocationService storeLocationService;

    @PostMapping(value = "/queryLocationPage")
    @ResponseBody
    public RestfulResponse<Page<StoreLocation>> queryLocationPage(AppQueryVo page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<Page<StoreLocation>> result = new RestfulResponse<>(0, "success");
        Page<StoreLocation> locDTOPage = new Page<>();
        Map<String, Object> map = new HashMap<>();
        map.put("storeHouseId", page.getHouseId());
        map.put("key", page.getKey());
        map.put("status", TableStatusEnum.STATUS_10.getCode());
        locDTOPage.setCondition(map);
        locDTOPage.setSize(page.getSize());
        locDTOPage.setCurrent(page.getCurrent());
        Page<StoreLocation> resultPage = storeLocationService.getUsableLocationByPage(locDTOPage);
        result.setData(resultPage);
        return result;
    }

}
