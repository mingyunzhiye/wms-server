package com.zhiche.wms.app.controller.opbaas;


import com.zhiche.wms.domain.model.opbaas.OpQrCodeBind;
import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.opbaas.paramdto.QrCodeParamDTO;
import com.zhiche.wms.service.opbaas.IQrCodeBindService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 二维码绑定 前端控制器
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Api(value = "qrCode-Bind", description = "绑定二维码接口操作")
@RestController
@RequestMapping("/model/qrCodeBind")
public class QrCodeBindController {

    private IQrCodeBindService iQrCodeBindService;

    @Autowired
    public void setiQrCodeBindService(IQrCodeBindService iQrCodeBindService) {
        this.iQrCodeBindService = iQrCodeBindService;
    }

    /**
     * <p>
     * 任务赋码 -- 重新赋码
     * </p>
     *
     * @param dto 参数封装
     */
    @PostMapping("/bindCodeAll")
    @ApiOperation(value = "绑定二维码信息(赋码)", notes = "司机操作绑定二维码信息")
    public ResultDTO<OpQrCodeBind> updateBindQrCode(QrCodeParamDTO dto) {
        ResultDTO<OpQrCodeBind> resultDTO = new ResultDTO<>(true, null, "操作成功!");
        iQrCodeBindService.updateBindQrCode(dto);
        return resultDTO;
    }

}

