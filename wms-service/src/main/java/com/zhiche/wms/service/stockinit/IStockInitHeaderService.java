package com.zhiche.wms.service.stockinit;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stockinit.StockInitHeader;
import com.zhiche.wms.dto.stockinit.StockInitQueryListResultDTO;

import java.util.Map;

/**
 * <p>
 * 期初库存头 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IStockInitHeaderService extends IService<StockInitHeader> {

    /**
     * 创建期初库存
     */
    boolean createStockInit(StockInitHeader stockInitHeader);

    /**
     * 审核期初库存
     */
    boolean auditStockInit(Long stockInitId);

    /**
     * 创建并审核期初库存
     */
    boolean createAndAuditStockInit(StockInitHeader stockInitHeader);

    /**
     * 撤销审核期初库存
     */
    boolean cancelAuditStockInit(Long stockInitId);


    /**
     * 查询期初库存列表
     */
    Page<StockInitQueryListResultDTO> queryStockPage(Page<StockInitQueryListResultDTO> page);

    /**
     * 页面导入期初库存接口
     */
    Map<String, JSONArray> saveImportStockInitNew(String data);
}
