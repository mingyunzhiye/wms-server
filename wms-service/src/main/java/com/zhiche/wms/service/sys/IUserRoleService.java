package com.zhiche.wms.service.sys;

import com.zhiche.wms.domain.model.sys.UserRole;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 用户角色关联表 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
public interface IUserRoleService extends IService<UserRole> {

    List<UserRole> listUserRole(Integer userId);
}
