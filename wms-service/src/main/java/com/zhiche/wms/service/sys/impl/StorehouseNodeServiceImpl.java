package com.zhiche.wms.service.sys.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.base.StorehouseNodeMapper;
import com.zhiche.wms.domain.model.base.StorehouseNode;
import com.zhiche.wms.service.sys.IStorehouseNodeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
@Service
public class StorehouseNodeServiceImpl extends ServiceImpl<StorehouseNodeMapper, StorehouseNode> implements IStorehouseNodeService {

}
