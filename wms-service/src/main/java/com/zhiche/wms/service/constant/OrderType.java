package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/14.
 */
public class OrderType {

    /**
     * 入库
     */
    public static final String INBOUND = "10";

    /**
     * 出库
     */
    public static final String OUTBOUND = "20";

    /**
     * 移库
     */
    public static final String MOVEBOUND = "30";

    /**
     * 其他
     */
    public static final String OTHER = "40";
}
