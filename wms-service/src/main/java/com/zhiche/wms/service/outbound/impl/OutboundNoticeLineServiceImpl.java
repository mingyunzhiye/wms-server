package com.zhiche.wms.service.outbound.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.outbound.OutboundNoticeLineMapper;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.dto.outbound.OutboundNoticeDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQrCodeResultDTO;
import com.zhiche.wms.service.opbaas.ExceptionToOTMService;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 出库通知明细 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@Service
public class OutboundNoticeLineServiceImpl extends ServiceImpl<OutboundNoticeLineMapper, OutboundNoticeLine> implements IOutboundNoticeLineService {

    @Autowired
    private IOtmOrderReleaseService releaseService;

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private ExceptionToOTMService exceptionToOTMService;

    @Override
    public boolean updateOutboundStatus(Long headerId) {
        return baseMapper.updateStatus(headerId) > 0;
    }

    @Override
    public Page<OutboundNoticeDTO> selectOutboundsPage(String key, Long houseId, Integer size, Integer current) {
        Page<OutboundNoticeDTO> page = new Page<>(current, size);
        EntityWrapper<OutboundNoticeDTO> ew = new EntityWrapper<>();
        ew.eq("store_house_id", houseId)
                .notIn("status", TableStatusEnum.STATUS_50.getCode())
                .andNew()
                .like("owner_order_no", key)
                .or()
                .like("lot_No1", key)
                .or()
                .eq("qr_code", key)
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        page.setRecords(this.baseMapper.queryPageNotice(page, ew));
        return page;
    }

    /**
     * 根据map查询二维码列表信息
     */
    @Override
    public List<OutboundShipQrCodeResultDTO> selectQrCodeInfoByParams(HashMap<String, Object> params) {
        return baseMapper.selectQrCodeInfoByParams(params);
    }

    /**
     *
     */
    @Override
    public List<OutboundNoticeLine> pageOutboundNotice(Page<OutboundNoticeLine> page, EntityWrapper<OutboundNoticeLine> outEW) {
        return baseMapper.pageOutboundNotice(page, outEW);
    }

    @Override
    public int selectCountWithHeader(EntityWrapper<OutboundNoticeLine> nlEW) {
        return baseMapper.selectCountWithHeader(nlEW);
    }

    @Override
    public OutboundNoticeDTO selectOutbound(Long LineId, Long houseId) {
        OutboundNoticeDTO noticeByLineId = this.baseMapper.getNoticeByLineId(LineId, houseId);
        //查询该车是否异常发运
        String send = exceptionToOTMService.isSend(noticeByLineId.getLotNo1(), noticeByLineId.getStoreHouseName());
        if (!StringUtils.isEmpty(send)) noticeByLineId.setIsCanSend(send);
        return noticeByLineId;
    }

    @Override
    public OutboundNoticeDTO selectInboundByQrCode(String key, Long houseId) {
        if (Strings.isNullOrEmpty(key)) {
            throw new BaseException("订单号或车架号为空");
        }
        EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
        oorEW.eq("qr_code", key)
                .ne("status", TableStatusEnum.STATUS_50.getCode())
                .orderBy("id", false);
        OtmOrderRelease release = releaseService.selectOne(oorEW);
        if (release != null) {
            key = release.getVin();
        }
        OutboundNoticeDTO noticeByKey = this.baseMapper.getNoticeByKey(key, houseId);
        //查询该车是否异常发运
        String send = exceptionToOTMService.isSend(noticeByKey.getLotNo1(), noticeByKey.getStoreHouseName());
        if (!StringUtils.isEmpty(send)) noticeByKey.setIsCanSend(send);
        return noticeByKey;
    }

    @Override
    public List<OutboundNoticeDTO> selectOutboundByHeadId(Long headId, Long houseIds) {
        try {
            EntityWrapper<OutboundNoticeDTO> ew = new EntityWrapper<>();
            //增加模糊条件
            ew.eq("header_id", headId).eq("store_house_id", houseIds);
            //设值
            return this.baseMapper.queryListNotice(ew);
        } catch (Exception e) {
            LOGGER.error(e.toString());
            throw new BaseException("信息查询出现异常");
        }
    }


}
