package com.zhiche.wms.service.common;

import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ShipParamDTO;

/**
 * 对外接口方法业务
 */
public interface IntegrationService {
    OTMEvent getOtmEvent(String key,
                         String releaseGid,
                         String eventType,
                         String shipmentGid,
                         String describe);

    void insertExportLog(String key,
                         OTMEvent event,
                         String res,
                         String describe,
                         String exportType);

    void insertExportLogNew(String valueOf,
                            ShipParamDTO paramDTO,
                            String res,
                            String s,
                            String code);
}
