package com.zhiche.wms.service.stock.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.stock.SkuMapper;
import com.zhiche.wms.domain.model.stock.Sku;
import com.zhiche.wms.domain.model.stock.SkuProperty;
import com.zhiche.wms.service.stock.ISkuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * 库存量单位(Stock Keeping Unit) 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private SnowFlakeId snowFlakeId;

    @Override
    public Sku getUniqueSku(SkuProperty skuProperty) {
        EntityWrapper<Sku> ew = new EntityWrapper<>();
        // 调整skuProperty取消客户的限制
        //if(Objects.isNull(skuProperty.getOwnerId())){
        //    ew.isNull("owner_id");
        //}else {
        //    ew.eq("owner_id",skuProperty.getOwnerId());
        //}

//        if(Objects.isNull(skuProperty.getMaterielId())){
//            ew.isNull("materiel_id");
//        }else {
//            ew.eq("materiel_id",skuProperty.getMaterielId());
//        }

        if(Objects.isNull(skuProperty.getUom())){
            ew.isNull("uom");
        }else {
            ew.eq("uom",skuProperty.getUom());
        }

        if(Objects.isNull(skuProperty.getLotNo0())){
            ew.isNull("lot_No0");
        }else {
            ew.eq("lot_No0",skuProperty.getLotNo0());
        }

        if(Objects.isNull(skuProperty.getLotNo1())){
            ew.isNull("lot_No1");
        }else {
            ew.eq("lot_No1",skuProperty.getLotNo1());
        }

        if(Objects.isNull(skuProperty.getLotNo2())){
            ew.isNull("lot_No2");
        }else {
            ew.eq("lot_No2",skuProperty.getLotNo2());
        }

        if(Objects.isNull(skuProperty.getLotNo3())){
            ew.isNull("lot_No3");
        }else {
            ew.eq("lot_No3",skuProperty.getLotNo3());
        }

        if(Objects.isNull(skuProperty.getLotNo4())){
            ew.isNull("lot_No4");
        }else {
            ew.eq("lot_No4",skuProperty.getLotNo4());
        }

        if(Objects.isNull(skuProperty.getLotNo5())){
            ew.isNull("lot_No5");
        }else {
            ew.eq("lot_No5",skuProperty.getLotNo5());
        }

        if(Objects.isNull(skuProperty.getLotNo6())){
            ew.isNull("lot_No6");
        }else {
            ew.eq("lot_No6",skuProperty.getLotNo6());
        }

        if(Objects.isNull(skuProperty.getLotNo7())){
            ew.isNull("lot_No7");
        }else {
            ew.eq("lot_No7",skuProperty.getLotNo7());
        }

        if(Objects.isNull(skuProperty.getLotNo8())){
            ew.isNull("lot_No8");
        }else {
            ew.eq("lot_No8",skuProperty.getLotNo8());
        }

        if(Objects.isNull(skuProperty.getLotNo9())){
            ew.isNull("lot_No9");
        }else {
            ew.eq("lot_No9",skuProperty.getLotNo9());
        }
        Sku sku = selectOne(ew);
        if(Objects.equals(null,sku)){
            sku = new Sku();
            //效率低
            //BeanUtils.copyProperties(skuProperty,sku);
            sku.setOwnerId(skuProperty.getOwnerId());
            sku.setMaterielId(skuProperty.getMaterielId());
            sku.setUom(skuProperty.getUom());
            sku.setLotNo0(skuProperty.getLotNo0());
            sku.setLotNo1(skuProperty.getLotNo1());
            sku.setLotNo2(skuProperty.getLotNo2());
            sku.setLotNo3(skuProperty.getLotNo3());
            sku.setLotNo4(skuProperty.getLotNo4());
            sku.setLotNo5(skuProperty.getLotNo5());
            sku.setLotNo6(skuProperty.getLotNo6());
            sku.setLotNo7(skuProperty.getLotNo7());
            sku.setLotNo8(skuProperty.getLotNo8());
            sku.setLotNo9(skuProperty.getLotNo9());
            sku.setId(snowFlakeId.nextId());
            try{
                boolean result = insert(sku);
                if(result){
                    return sku;
                }else{
                    throw new BaseException(1001,"保存SKU失败!");
                }
            }catch (Exception e){
                throw e;
            }
        }else{
            return sku;
        }
    }
}
