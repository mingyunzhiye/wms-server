package com.zhiche.wms.service.outbound.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.outbound.OutboundPrepareLineMapper;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.dto.outbound.OutboundPrepareDTO;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundPrepareLineService;
import com.zhiche.wms.service.sys.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 出库备料单明细 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
@Service
public class OutboundPrepareLineServiceImpl extends ServiceImpl<OutboundPrepareLineMapper, OutboundPrepareLine> implements IOutboundPrepareLineService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserService userService;
    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;
    @Autowired
    private IOtmOrderReleaseService releaseService;

    @Override
    public List<OutboundPrepareDTO> queryPageByLotId(String key, String houseid, Integer current, Integer size) {
        //if (StringUtils.isBlank(key)) {
        //    throw new BaseException("参数不能为空");
        //}
        if (StringUtils.isBlank(houseid)) {
            throw new BaseException("仓库id不能为空");
        }
        if (StringUtils.isNotBlank(key)) {
            EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
            oorEW.eq("qr_code", key)
                    .ne("status", TableStatusEnum.STATUS_50.getCode())
                    .orderBy("id", false);
            OtmOrderRelease release = releaseService.selectOne(oorEW);
            if (release != null) {
                key = release.getVin();
            }
        }
        Page<OutboundPrepareDTO> page = new Page<>(current, size);
        EntityWrapper<OutboundPrepareDTO> ew = new EntityWrapper<>();
        ew.eq("store_house_id", houseid)
                .ne("status", TableStatusEnum.STATUS_50.getCode());
        if (StringUtils.isNotBlank(key)) {
            ew.andNew()
                    .like("owner_order_no", key)
                    .or()
                    .like("lot_No1", key)
                    .or()
                    .eq("qr_code", key);
        }
        ew.orderBy("id", false);
        return baseMapper.queryPage(page, ew);
    }

    @Override
    public OutboundPrepareDTO getPrepare(String key, Long houseId) {
        try {
            if (StringUtils.isBlank(key)) {
                throw new BaseException("参数不能为空");
            }
            return baseMapper.getPrepare(new Long(key), houseId);
        } catch (NumberFormatException e) {
            LOGGER.error(e.toString());
            throw new BaseException("key非数字");
        } catch (Exception e) {
            LOGGER.error(e.toString());
            throw new BaseException("获取备料信息出错");
        }
    }

    @Override
    public OutboundPrepareDTO getPrepareBykey(String key, Long houseId) {
        try {
            if (StringUtils.isBlank(key)) {
                throw new BaseException("扫码信息不能为空");
            }
            EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
            oorEW.eq("qr_code", key)
                    .ne("status", TableStatusEnum.STATUS_50.getCode())
                    .orderBy("id", false);
            OtmOrderRelease release = releaseService.selectOne(oorEW);
            if (release != null) {
                key = release.getVin();
            }
            return baseMapper.getPrepareBykey(key, houseId);
        } catch (Exception e) {
            LOGGER.error(e.toString());
            throw new BaseException("获取备料信息出错");
        }
    }

    @Override
    public OutboundPrepareDTO updateStatus(String key, Long houseId) {
        try {
            if (Strings.isNullOrEmpty(key)) throw new BaseException("id号为空");
            OutboundPrepareLine line = this.selectById(Long.valueOf(key));
            if (line == null) {
                throw new BaseException("该备料明细已删除");
            }
            if (TableStatusEnum.STATUS_50.getCode().equals(line.getStatus())) {
                throw new BaseException("该备料明细已取消");
            }
            User user = userService.getLoginUser();
            OutboundPrepareLine upLine = new OutboundPrepareLine();
            upLine.setId(line.getId());
            upLine.setStatus(TableStatusEnum.STATUS_20.getCode());
            upLine.setGmtModified(new Date());
            upLine.setStartTime(new Date());
            if (user != null) {
                upLine.setPreparetor(user.getName());
            }
            baseMapper.updateById(upLine);
            //更新头开始
            EntityWrapper<OutboundPrepareHeader> headerEW = new EntityWrapper<>();
            headerEW.eq("id", line.getHeaderId())
                    .ne("status", TableStatusEnum.STATUS_50.getCode());
            OutboundPrepareHeader header = new OutboundPrepareHeader();
            header.setStartTime(new Date());
            header.setStatus(TableStatusEnum.STATUS_20.getCode());
            prepareHeaderService.update(header, headerEW);
            return this.getPrepare(key, houseId);
        } catch (Exception e) {
            LOGGER.error(e.toString());
            throw new BaseException("修改备料信息出错");
        }
    }
}
