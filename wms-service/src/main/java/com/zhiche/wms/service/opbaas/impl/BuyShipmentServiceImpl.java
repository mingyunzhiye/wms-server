package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.otm.OtmShipmentMapper;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.service.opbaas.IBuyShipmentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 调度指令 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Service
public class BuyShipmentServiceImpl extends ServiceImpl<OtmShipmentMapper, OtmShipment> implements IBuyShipmentService {

}
