package com.zhiche.wms.service.movement;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.movement.MovementHeader;

/**
 * <p>
 * 移位单头 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IMovementHeaderService extends IService<MovementHeader> {

    /**
     * 创建移位单
     * @return
     */
    boolean creatMovement(MovementHeader movementHeader);

    /**
     * 根据ID审核移位单
     * @return
     */
    boolean auditMovement(Long movementId);

    /**
     * 创建并审核移位单
     * @param movementHeader
     * @return
     */
    boolean createAndAuditMovenment(MovementHeader movementHeader);

    /**
     * 撤销审核移位单
     * @param movementId
     * @return
     */
    boolean cancelAuditMovement(Long movementId);

}
