package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundShipHeader;
import com.zhiche.wms.domain.model.outbound.OutboundShipLine;
import com.zhiche.wms.dto.outbound.OutboundShipDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQuitResultDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库单明细 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IOutboundShipLineService extends IService<OutboundShipLine> {

    OutboundShipDTO getPutWaylineById(Long id);

    List<OutboundShipQuitResultDTO> selectQuitInfoByParams(HashMap<String, Object> params);

    /**
     * 查询出库记录
     */
    Page<OutboundShipDTO> queryOutShipLinePage(Page<OutboundShipDTO> page);

    /**
     * 出库记录导出
     */
    List<OutboundShipDTO> queryExportOSData(Map<String, String> condition);

    /**
     * 按通知单明细行ID全部出库
     */
    OutboundShipHeader shipByNoticeLineId(Long noticeLineId, String outboundType, String genMethod);
}
