package com.zhiche.wms.service.log.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.log.ItfExplogLineMapper;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.service.log.IItfExplogLineService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 接口导出日志明细 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@Service
public class ItfExplogLineServiceImpl extends ServiceImpl<ItfExplogLineMapper, ItfExplogLine> implements IItfExplogLineService {

}
