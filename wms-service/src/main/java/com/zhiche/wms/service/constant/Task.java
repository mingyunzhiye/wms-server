package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/17.
 */
public class Task {

    public static class Type{
        /**
         * 寻车
         */
        public static final String SEEK = "10";

        /**
         * 移车
         */
        public static final String MOVE = "20";

        /**
         * 提车
         */
        public static final String PICK = "30";
    }

    public static class Status{
        /**
         * 创建
         */
        public static final String CREATED = "10";

        /**
         * 开始
         */
        public static final String START = "20";

        /**
         * 完成
         */
        public static final String FINISH = "30";

        /**
         * 取消
         */
        public static final String CANCEL = "50";
    }

}
