package com.zhiche.wms.service.outbound.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.outbound.OutboundPrepareHeaderMapper;
import com.zhiche.wms.domain.model.base.StoreLocation;
import com.zhiche.wms.domain.model.inbound.InboundPutawayLine;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;
import com.zhiche.wms.domain.model.stock.Sku;
import com.zhiche.wms.domain.model.stock.Stock;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.dto.outbound.*;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.base.IStoreLocationService;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundPrepareLineService;
import com.zhiche.wms.service.stock.ISkuService;
import com.zhiche.wms.service.stock.IStockService;
import com.zhiche.wms.service.sys.IUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 出库备料单头 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
@Service
public class OutboundPrepareHeaderServiceImpl extends ServiceImpl<OutboundPrepareHeaderMapper, OutboundPrepareHeader> implements IOutboundPrepareHeaderService {

    @Autowired
    private IUserService userService;
    @Autowired
    private IOutboundNoticeLineService noticeLineService;
    @Autowired
    private IOutboundNoticeHeaderService noticeHeaderService;
    @Autowired
    private IOutboundPrepareLineService prepareLineService;
    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private IBusinessDocNumberService docNumberService;
    @Autowired
    private IStockService stockService;
    @Autowired
    private IStoreLocationService locationService;
    @Autowired
    private ISkuService skuService;

    public Integer updateStatus(Long id, Long houseId, Integer status, BigDecimal damagedSum) {
        return this.baseMapper.updateStatus(id, houseId, status, damagedSum);
    }

    /**
     * 查询备料任务列表  关联通知单头/明细  出库单头/明细
     */
    @Override
    public Page<PreparationListHeadDTO> queryOutPreparePage(Page<PreparationListHeadDTO> page) {
        Wrapper<PreparationListHeadDTO> wp = buildWrapper(page,TableStatusEnum.STATUS_0.getCode());
        List<PreparationListHeadDTO> records= baseMapper.queryOutPreparePage(page, wp);
        page.setRecords(records);
        return page;
    }

    private Wrapper<PreparationListHeadDTO> buildWrapper(Page<PreparationListHeadDTO> page,String is) {
        if (page == null) {
            throw new BaseException("参数不能为空");
        }
        Wrapper<PreparationListHeadDTO> wp = new EntityWrapper<>();
        Map cd = page.getCondition();
        if (cd != null) {
            if (Objects.nonNull(cd.get("storeHouseId")) &&
                    StringUtils.isNotBlank(cd.get("storeHouseId").toString())) {
                wp.eq("houseId", cd.get("storeHouseId").toString());
            }
            if (TableStatusEnum.STATUS_0.getCode().equals(is)) {
                if (Objects.nonNull(cd.get("preparationStatus")) &&
                        StringUtils.isNotBlank(cd.get("preparationStatus").toString())) {
                    wp.eq("prepareStatus", cd.get("preparationStatus").toString());
                }
                //默认查询状态
                if (Objects.isNull(cd.get("preparationStatus")) ||
                        StringUtils.isBlank(cd.get("preparationStatus").toString())) {
                    ArrayList<String> status = Lists.newArrayList();
                    status.add(TableStatusEnum.STATUS_10.getCode());
                    status.add(TableStatusEnum.STATUS_20.getCode());
                    wp.in("prepareStatus", status);
                }
            }
            if (Objects.nonNull(cd.get("ownerOrderNo")) &&
                    StringUtils.isNotBlank(cd.get("ownerOrderNo").toString())) {
                wp.eq("owner_order_no", cd.get("ownerOrderNo").toString());
            }
            if (Objects.nonNull(cd.get("outShipStatus")) &&
                    StringUtils.isNotBlank(cd.get("outShipStatus").toString())) {
                wp.eq("noticeHeadStatus", cd.get("outShipStatus").toString());
            }
            if (Objects.isNull(cd.get("outShipStatus")) ||
                    StringUtils.isBlank(cd.get("outShipStatus").toString())) {
                ArrayList<String> status = Lists.newArrayList();
                status.add(TableStatusEnum.STATUS_10.getCode());
                status.add(TableStatusEnum.STATUS_20.getCode());
                wp.in("noticeHeadStatus", status);
            }
            if (Objects.nonNull(cd.get("lotNo1")) &&
                    StringUtils.isNotBlank(cd.get("lotNo1").toString())) {
                EntityWrapper<OutboundNoticeLine> nlEW = new EntityWrapper<>();
                String vin = cd.get("lotNo1").toString();
                String[] split = vin.split(",");
                List<String> vins = Arrays.asList(split);
                nlEW.in("lot_no1", vins).orNew().like("lot_no1", vin)
                        .ne("status", TableStatusEnum.STATUS_50.getCode());
                List<OutboundNoticeLine> noticeLines = noticeLineService.selectList(nlEW);
                ArrayList<Long> ids = Lists.newArrayList();
                for (OutboundNoticeLine line:noticeLines) {
                    ids.add(line.getHeaderId());
                }
                wp.in("noticeHeadId", ids);
            }
        }
        if (TableStatusEnum.STATUS_1.getCode().equals(is)) {
            wp.groupBy("owner_order_no");
        }
        wp.orderBy("formatCreate", Boolean.FALSE)
                .orderBy("owner_order_no", Boolean.TRUE);
        return wp;
    }


    /**
     * 保存备料单
     */
    @Override
    public void createPreparation(OutboundPreparationParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空!");
        }
        String noticeHeadId = dto.getNoticeHeadId();
        String planTime = dto.getPlanTime();
        String prepareLane = dto.getPrepareLane();
        if (StringUtils.isBlank(prepareLane)) {
            throw new BaseException("请填写备料装车道");
        }
        if (StringUtils.isBlank(planTime)) {
            throw new BaseException("计划备料时间不能为空");
        }
        if (StringUtils.isBlank(noticeHeadId)) {
            throw new BaseException("请选择需要创建备料任务的数据");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date pt = null;
        try {
            pt = sdf.parse(planTime);
        } catch (ParseException e) {
            throw new BaseException("日期" + dto.getPlanTime() + "转换异常");
        }

        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        OutboundNoticeHeader noticeHeader = noticeHeaderService.selectById(noticeHeadId);
        //校验重复性
        EntityWrapper<OutboundPrepareHeader> wrapper = new EntityWrapper<>();
        wrapper.in("notice_id", noticeHeadId);
        int count = selectCount(wrapper);
        if (count > 0) {
            throw new BaseException("该通知单headId:" + noticeHeadId + "存在已经创建备料的数据或已生成单台备料,如已操作单台备料,请继续单台备料操作");
        }
        EntityWrapper<OutboundNoticeLine> nlw = new EntityWrapper<>();
        nlw.eq("header_id", noticeHeadId)
                .eq("status", TableStatusEnum.STATUS_10.getCode());
        List<OutboundNoticeLine> noticeLines = noticeLineService.selectList(nlw);
        if (CollectionUtils.isEmpty(noticeLines)) {
            throw new BaseException("未查询到通知单headId:" + noticeHeadId + "的出库通知单信息");
        }
        ArrayList<OutboundPrepareLine> ipls = Lists.newArrayList();
        Date finalPt = pt;
        OutboundPrepareHeader ph = savePreparationHead(loginUser, finalPt, noticeHeader);
        ph.setPlanSumQty(new BigDecimal(0));
        ph.setLineCount(0);
        ph.setLoadingArea(prepareLane);
        for (OutboundNoticeLine noticeLine : noticeLines) {
            List<StockSkuToPrepareDTO> list = stockService.selectStockToPrepareByParams(noticeLine, noticeHeader.getStoreHouseId());
            if (CollectionUtils.isEmpty(list)) {
                throw new BaseException("未查询到NoticeLineId:" + noticeLine.getId() + "库存信息");
            }
            StockSkuToPrepareDTO stockSkuToPrepareDTO = list.get(0);
            if ((stockSkuToPrepareDTO.getRemainQty().compareTo(noticeLine.getExpectQty())) < 0) {
                throw new BaseException("库存id:" + stockSkuToPrepareDTO.getStockId() + "库存不足通知单数量出库");
            }
            OutboundPrepareLine pl = savePreparationLine(
                    loginUser,
                    finalPt,
                    noticeLine,
                    ph,
                    stockSkuToPrepareDTO);
            pl.setShipSpace(prepareLane);
            ph.setPlanSumQty(ph.getPlanSumQty().add(pl.getPlanQty()));
            ph.setLineCount(ph.getLineCount() + 1);
            ipls.add(pl);
        }
        //头
        if (CollectionUtils.isNotEmpty(ipls)) {
            insert(ph);
            prepareLineService.insertBatch(ipls);
        }
    }

    /**
     * 根据noticeHeadId获取详细信息
     */
    @Override
    public List<PreparationListDetailDTO> getPreparationDetail(OutboundPreparationParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        String noticeHeadId = dto.getNoticeHeadId();
        if (StringUtils.isBlank(noticeHeadId)) {
            throw new BaseException("通知单id不能为空");
        }
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("noticeHeadId", noticeHeadId);
        params.put("headId", dto.getHeadId());
        params.put("groupBy", "lineId");
        params.put("orderBy", "a.gmt_modified desc");
        baseMapper.updateSQLMode();
        List<PreparationListDetailDTO> detailDTOS = baseMapper.selectPreparationDetailListByParams(params);
        List<PreparationListDetailDTO> resluts=new ArrayList<>();
        if (StringUtils.isBlank(dto.getHeadId())) {
            for (PreparationListDetailDTO detailDTO : detailDTOS) {
                Wrapper<OutboundPrepareLine> outboundPrepareLineWrapper = new EntityWrapper<>();
                outboundPrepareLineWrapper.eq("notice_line_id", detailDTO.getLineId());
                OutboundPrepareLine outboundPrepareLine = prepareLineService.selectOne(outboundPrepareLineWrapper);
                if (Objects.isNull(outboundPrepareLine)) {
                    resluts.add(detailDTO);
                }
            }
            if (CollectionUtils.isNotEmpty(resluts)) {
                for (PreparationListDetailDTO reslut:resluts) {
                    EntityWrapper<Sku> skuEW = new EntityWrapper<>();
                    skuEW.eq("lot_no1", reslut.getLotNo1()).orderBy("id", false);
                    Sku sku = skuService.selectOne(skuEW);
                    EntityWrapper<Stock> stcEW = new EntityWrapper<>();
                    if (sku != null) {
                        stcEW.eq("sku_id", sku.getId()).orderBy("id", false);
                        Stock stock = stockService.selectOne(stcEW);
                        if (stock != null) {
                            StoreLocation location = locationService.selectById(stock.getLocationId());
                            if (location != null) {
                                reslut.setLocationId(String.valueOf(location.getId()));
                                reslut.setLocationNo(location.getCode());
                            }
                        }
                    }
                }
                return resluts;
            }
        }
        if (CollectionUtils.isNotEmpty(detailDTOS)) {
            for (PreparationListDetailDTO detail:detailDTOS) {
                EntityWrapper<Sku> skuEW = new EntityWrapper<>();
                skuEW.eq("lot_no1", detail.getLotNo1())
                        .orderBy("id", false);
                Sku sku = skuService.selectOne(skuEW);
                EntityWrapper<Stock> stcEW = new EntityWrapper<>();
                if (sku != null) {
                    stcEW.eq("sku_id", sku.getId())
                            .orderBy("id", false);
                    Stock stock = stockService.selectOne(stcEW);
                    if (stock != null) {
                        StoreLocation location = locationService.selectById(stock.getLocationId());
                        if (location != null) {
                            detail.setLocationId(String.valueOf(location.getId()));
                            detail.setLocationNo(location.getCode());
                        }
                    }
                }
            }
            return detailDTOS;
        }
        return null;
    }

    /**
     * 备料任务变更   已出库不支持
     */
    @Override
    public void updateChangePreparation(OutboundPreparationParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        String noticeHeadId = dto.getNoticeHeadId();
        String storeHouseId = dto.getStoreHouseId();
        if (StringUtils.isBlank(noticeHeadId)) {
            throw new BaseException("请选择需要变更的备料计划");
        }
        if (StringUtils.isBlank(storeHouseId)) {
            throw new BaseException("仓库id不能为空");
        }
        EntityWrapper<OutboundNoticeHeader> hlw = new EntityWrapper<>();
        hlw.eq("id", noticeHeadId);
        OutboundNoticeHeader noticeHeader = noticeHeaderService.selectById(noticeHeadId);
        if (noticeHeader == null) {
            throw new BaseException("未查询到通知单头Id:" + noticeHeadId + "的通知单信息");
        }
        //查询备料头
        EntityWrapper<OutboundPrepareHeader> phew = new EntityWrapper<>();
        phew.eq("notice_id", noticeHeadId);
        List<OutboundPrepareHeader> prepareHeaders = baseMapper.selectList(phew);
        if (CollectionUtils.isEmpty(prepareHeaders)) {
            throw new BaseException("该通知单头ID:" + noticeHeadId + "还未生成备料");
        }
        OutboundPrepareHeader prepareHeader = prepareHeaders.get(0);
        EntityWrapper<OutboundNoticeLine> llw = new EntityWrapper<>();
        llw.eq("header_id", noticeHeadId);
        List<OutboundNoticeLine> noticeLines = noticeLineService.selectList(llw);
        if (CollectionUtils.isNotEmpty(noticeLines)) {
            noticeLines.forEach(v -> {
                String nls = v.getStatus();
                if (TableStatusEnum.STATUS_50.getCode().equals(nls)) {
                    //看备料状态明细 如果是已完成  30,就不能调整为已取消。
                    EntityWrapper<OutboundPrepareLine> plew = new EntityWrapper<>();
                    plew.eq("notice_line_id", v.getId());
                    List<OutboundPrepareLine> prepareLines = prepareLineService.selectList(plew);
                    if (CollectionUtils.isNotEmpty(prepareLines)) {
                        for (OutboundPrepareLine pl : prepareLines) {
                            if (TableStatusEnum.STATUS_30.getCode().equals(pl.getStatus())) {
                                throw new BaseException("备料明细Id:" + pl.getId() + "已经进行过出库作业,请进行退库!");
                            }
                            pl.setStatus(TableStatusEnum.STATUS_40.getCode());
                            pl.setGmtModified(null);
                            prepareLineService.updateById(pl);
                        }
                    }
                } else {
                    //看备料状态明细 如果没有明细就新增
                    EntityWrapper<OutboundPrepareLine> plew = new EntityWrapper<>();
                    plew.eq("notice_line_id", v.getId());
                    List<OutboundPrepareLine> prepareLines = prepareLineService.selectList(plew);
                    if (CollectionUtils.isEmpty(prepareLines)) {
                        //新增明细
                        OutboundPrepareLine prepareLine = new OutboundPrepareLine();
                        BeanUtils.copyProperties(v, prepareLine);
                        prepareLine.setNoticeLineId(v.getId());
                        prepareLine.setStatus(TableStatusEnum.STATUS_10.getCode());
                        List<StockSkuToPrepareDTO> ssDTO = stockService.selectStockToPrepareByParams(v, Long.valueOf(storeHouseId));
                        if (CollectionUtils.isEmpty(ssDTO)) {
                            throw new BaseException("未查询到" + v.getLotNo1() + "的库存信息");
                        }
                        StockSkuToPrepareDTO stockSkuToPrepareDTO = ssDTO.get(0);
                        if ((stockSkuToPrepareDTO.getRemainQty().compareTo(v.getExpectQty())) < 0) {
                            throw new BaseException("库存id:" + stockSkuToPrepareDTO.getStockId() + "库存不足通知单数量出库");
                        }
                        OutboundPrepareLine pl = savePreparationLine(
                                loginUser,
                                new Date(),
                                v,
                                prepareHeader,
                                stockSkuToPrepareDTO);
                        pl.setShipSpace(prepareHeader.getLoadingArea());
                        prepareLineService.insert(pl);
                        //通知单全部出库-->部分出库
                        if (TableStatusEnum.STATUS_30.getCode().equals(noticeHeader.getStatus())) {
                            noticeHeader.setStatus(TableStatusEnum.STATUS_20.getCode());
                            noticeHeader.setUserModified(loginUser.getName());
                            noticeHeaderService.updateById(noticeHeader);
                        }
                        if (TableStatusEnum.STATUS_40.getCode().equals(prepareHeader.getStatus())) {
                            prepareHeader.setStatus(TableStatusEnum.STATUS_30.getCode());
                        }
                        prepareHeader.setPlanSumQty(prepareHeader.getPlanSumQty());
                        prepareHeader.setLineCount(prepareHeader.getLineCount() + 1);
                        prepareHeader.setUserModified(loginUser.getName());
                        prepareHeader.setGmtModified(null);
                        baseMapper.updateById(prepareHeader);
                    }
                }
            });
        }

    }

    @Override
    @Transactional
    public void updatePrepareFinishByLineId(Long noticeLineId, String modifiedName) {
        EntityWrapper<OutboundPrepareLine> plWp = new EntityWrapper<>();
        //过滤已取消
        plWp.eq("notice_line_id", noticeLineId);
        List<OutboundPrepareLine> opls = prepareLineService.selectList(plWp);
        boolean isAllFinish = Boolean.TRUE;

        if (CollectionUtils.isNotEmpty(opls)) {
            OutboundPrepareLine prepareLine = opls.get(0);
            prepareLine.setFinishTime(new Date());
            prepareLine.setStatus(TableStatusEnum.STATUS_30.getCode());
            prepareLine.setActualQty(prepareLine.getPlanQty());
            prepareLine.setGmtModified(null);
            prepareLineService.updateById(prepareLine);
            //判断头状态
            OutboundPrepareHeader prepareHeader = selectById(prepareLine.getHeaderId());
            if (prepareHeader != null) {
                EntityWrapper<OutboundPrepareLine> ew = new EntityWrapper<>();
                ew.eq("header_id", prepareHeader.getId())
                        .notIn("status", TableStatusEnum.STATUS_40.getCode());
                List<OutboundPrepareLine> lines = prepareLineService.selectList(ew);
                for (OutboundPrepareLine line : lines) {
                    if (!TableStatusEnum.STATUS_30.getCode().equals(line.getStatus())) {
                        isAllFinish = Boolean.FALSE;
                        break;
                    }
                }
                if (isAllFinish) {
                    prepareHeader.setFinishTime(new Date());
                    prepareHeader.setStatus(TableStatusEnum.STATUS_40.getCode());
                } else {
                    prepareHeader.setStatus(TableStatusEnum.STATUS_30.getCode());
                }
                prepareHeader.setUserModified(modifiedName);
                prepareHeader.setGmtModified(null);
                updateById(prepareHeader);
            }
        }
    }

    private OutboundPrepareLine savePreparationLine(User loginUser,
                                                    Date finalPt,
                                                    OutboundNoticeLine nl,
                                                    OutboundPrepareHeader ph,
                                                    StockSkuToPrepareDTO stock) {
        OutboundPrepareLine pl = new OutboundPrepareLine();
        BeanUtils.copyProperties(nl, pl);
        pl.setId(snowFlakeId.nextId());
        pl.setHeaderId(ph.getId());
        pl.setNoticeLineId(nl.getId());
        pl.setLocationId(stock.getLocationId());
        pl.setStockId(stock.getStockId());
        StoreLocation storeLocation = locationService.selectById(stock.getLocationId());
        if (storeLocation != null) {
            pl.setLocationNo(storeLocation.getCode());
        }
        pl.setPlanQty(nl.getExpectQty());
        pl.setActualQty(new BigDecimal("0"));
        //pl.setPreparetor(loginUser.getName());
        pl.setPlanTime(finalPt);
        pl.setStatus(TableStatusEnum.STATUS_10.getCode());
        return pl;
    }

    private OutboundPrepareHeader savePreparationHead(User loginUser,
                                                      Date finalPt,
                                                      OutboundNoticeHeader nh) {
        OutboundPrepareHeader ph = new OutboundPrepareHeader();
        BeanUtils.copyProperties(nh, ph);
        ph.setId(snowFlakeId.nextId());
        ph.setNoticeId(nh.getId());
        ph.setPrepareNo(docNumberService.getOutboundPrepareNo());
        ph.setOrderTime(new Date());
        ph.setPlanTime(finalPt);
        ph.setStatus(TableStatusEnum.STATUS_20.getCode());
        ph.setUserCreate(loginUser.getName());
        ph.setUserModified(loginUser.getName());
        return ph;
    }

    private Map<String, Object> checkAndSetParams(OutboundPreparationParamDTO dto) {
        String preparationStatus = dto.getPreparationStatus();
        String outShipStatus = dto.getOutShipStatus();
        String headSourceNo = dto.getHeadSourceNo();
        String headSourceKey = dto.getHeadSourceKey();
        String startTime = dto.getStartTime();
        String endTime = dto.getEndTime();
        String noticeHeadNo = dto.getNoticeHeadNo();
        String ownerOrderNo = dto.getOwnerOrderNo();
        Map<String, Object> params = Maps.newHashMap();
        params.put("start", dto.getStartIndex());
        params.put("end", dto.getPageSize());
        params.put("orderBy", "b.gmt_create desc,b.id ASC");
        if (StringUtils.isNotBlank(preparationStatus)) {
            if (TableStatusEnum.STATUS_10.getCode().equals(preparationStatus)) {
                params.put("preparationStatus", "and (d.status = '" + preparationStatus + "' or d.status is null)");
            } else {
                params.put("preparationStatus", "and d.status = " + preparationStatus);
            }
        }
        if (StringUtils.isNotBlank(ownerOrderNo)) {
            params.put("ownerOrderNo", ownerOrderNo);
        }
        if (StringUtils.isNotBlank(noticeHeadNo)) {
            params.put("noticeHeadNo", noticeHeadNo);
        }
        if (StringUtils.isNotBlank(outShipStatus)) {
            params.put("outShipStatus", outShipStatus);
        }
        if (StringUtils.isNotBlank(headSourceNo)) {
            params.put("headSourceNo", headSourceNo);
        }
        if (StringUtils.isNotBlank(headSourceKey)) {
            params.put("headSourceKey", headSourceKey);
        }
        if (StringUtils.isNotBlank(startTime)) {
            params.put("startTime", startTime + " 00:00:00");
        }
        if (StringUtils.isNotBlank(endTime)) {
            params.put("endTime", endTime + " 23:59:59");
        }
        return params;
    }

    @Override
    public Page<PreparationListHeadDTO> notPreparationList(Page<PreparationListHeadDTO> page) {
        Wrapper<PreparationListHeadDTO> wp = buildWrapper(page,TableStatusEnum.STATUS_1.getCode());
        baseMapper.updateSQLMode();
        List<PreparationListHeadDTO> records= baseMapper.queryNotOutPreparePage(page,wp);
//        List<PreparationListHeadDTO> results=new ArrayList<>();
//        for (PreparationListHeadDTO record : records) {
//            HashMap<String, Object> params = Maps.newHashMap();
//            params.put("noticeHeadId", record.getNoticeHeadId());
//            params.put("groupBy", null);
//            params.put("orderBy", "a.gmt_modified desc");
//            List<PreparationListDetailDTO> detailDTOS = baseMapper.selectPreparationDetailListByParams(params);
//
//            for (PreparationListDetailDTO detailDTO : detailDTOS) {
//                Wrapper<OutboundPrepareLine> prepareLineWrapper=new EntityWrapper<>();
//                prepareLineWrapper.eq("notice_line_id",detailDTO.getLineId());
//                int count = prepareLineService.selectCount(prepareLineWrapper);
//                if (count<=0){
//                    results.add(record);
//                    break;
//                }
//            }
//        }
//        page.setTotal(results.size());
        page.setRecords(records);
        return page;
    }

    @Override
    public void singlePreparation(OutboundPreparationParamDTO dto) {
        if (dto == null) {
            throw new BaseException("参数不能为空!");
        }
        if (StringUtils.isBlank(dto.getPrepareLane())) {
            throw new BaseException("请填写备料装车道");
        }
        if (StringUtils.isBlank(dto.getPlanTime())) {
            throw new BaseException("计划备料时间不能为空");
        }
        if (StringUtils.isBlank(dto.getNoticeHeadId())) {
            throw new BaseException("请选择需要创建备料任务的数据");
        }
        if (dto.getLineIds().length<0){
            throw new BaseException("请选择需要单台备料任务的数据");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date pt = null;
        try {
            pt = sdf.parse(dto.getPlanTime());
        } catch (ParseException e) {
            throw new BaseException("日期" + dto.getPlanTime() + "转换异常");
        }

        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请登录后进行该操作");
        }
        OutboundNoticeHeader noticeHeader = noticeHeaderService.selectById(dto.getNoticeHeadId());
        String[] lineIds = dto.getLineIds();

        ArrayList<OutboundPrepareLine> ipls = Lists.newArrayList();
        Date finalPt = pt;
        OutboundPrepareHeader ph = savePreparationHead(loginUser, finalPt, noticeHeader);
        ph.setPlanSumQty(new BigDecimal(0));
        ph.setLineCount(0);
        ph.setLoadingArea(dto.getPrepareLane());
        //单台备料生成一个明细一个头
        for (String lineId : lineIds) {
            Wrapper<OutboundPrepareLine> outboundPrepareLineWrapper=new EntityWrapper<>();
            outboundPrepareLineWrapper.eq("notice_line_id",lineId);
            int count = prepareLineService.selectCount(outboundPrepareLineWrapper);
            if (count>0){
                throw new BaseException("该明细已进行过单台备料");
            }
            EntityWrapper<OutboundNoticeLine> nlw = new EntityWrapper<>();
            nlw.eq("id", lineId)
                .eq("status", TableStatusEnum.STATUS_10.getCode());
            OutboundNoticeLine outboundNoticeLine = noticeLineService.selectOne(nlw);
            if (Objects.isNull(outboundNoticeLine)) {
                throw new BaseException("未查询到通知单headId:" + lineId + "的出库通知单信息");
            }
                List<StockSkuToPrepareDTO> list = stockService.selectStockToPrepareByParams(outboundNoticeLine, noticeHeader.getStoreHouseId());
                if (CollectionUtils.isEmpty(list)) {
                    throw new BaseException("未查询到NoticeLineId:" + outboundNoticeLine.getId() + "库存信息");
                }
                StockSkuToPrepareDTO stockSkuToPrepareDTO = list.get(0);
                if ((stockSkuToPrepareDTO.getRemainQty().compareTo(outboundNoticeLine.getExpectQty())) < 0) {
                    throw new BaseException("库存id:" + stockSkuToPrepareDTO.getStockId() + "库存不足通知单数量出库");
                }
                OutboundPrepareLine pl = savePreparationLine(
                        loginUser,
                        finalPt,
                        outboundNoticeLine,
                        ph,
                        stockSkuToPrepareDTO);
                pl.setShipSpace(dto.getPrepareLane());
                ph.setPlanSumQty(ph.getPlanSumQty().add(pl.getPlanQty()));
                ph.setLineCount(ph.getLineCount() + 1);
                ipls.add(pl);
        }
        //头
        if (CollectionUtils.isNotEmpty(ipls)) {
            insert(ph);
            prepareLineService.insertBatch(ipls);
        }

    }
}
