package com.zhiche.wms.service.utils;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:17 2018/12/12
 */
public class CommonValueConstant {
    /**
     * 通知入库详情状态
     */
    public static final String INBOUND_NOTICE_LINE_10 = "10";

    /**
     * sys_config 无人值守code：UNATTENDED
     */
    public static final String CODE_UNATTENDED = "UNATTENDED";

    /**
     * 仓库查询类型 config_type: WAREHOUSE
     */
    public static final String CONFIG_TYPE_WAREHOUSE = "WAREHOUSE";

    /**
     * 配置是否勾选 1 已选择配置,0 未配置
     */
    public static final String CONFIGED_1 = "1";
}
