package com.zhiche.wms.service.otm;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 调度指令 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public interface IOtmShipmentService extends IService<OtmShipment> {

    /**
     * 保存调度指令
     */
    boolean insertShipment(OtmShipment otmShipment);

    /**
     * 装车发运-模糊查询指令列表  -- 关联发车配置点
     */
    List<ShipmentForShipDTO> queryShipmentReleaseList(Page<ShipmentForShipDTO> page, HashMap<String, Object> param, EntityWrapper<ShipmentForShipDTO> ew);

    /**
     * 装车发运-点击获取指令详情
     */
    List<ShipmentForShipDTO > getShipDetail(EntityWrapper<ShipmentForShipDTO> ew);

    int countShipmentReleaseList(HashMap<String, Object> params, EntityWrapper<ShipmentForShipDTO> ew);

    List<ShipmentForShipDTO> queryShipmentReleases(@Param("ew") EntityWrapper<ShipmentForShipDTO> dataEW);

}
