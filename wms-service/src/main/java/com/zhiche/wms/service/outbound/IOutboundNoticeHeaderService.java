package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.dto.outbound.OutNoticeListResultDTO;
import com.zhiche.wms.dto.outbound.OutboundNoticeHeaderDTO;
import com.zhiche.wms.dto.outbound.OutboundNoticeParamDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库通知单头 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IOutboundNoticeHeaderService extends IService<OutboundNoticeHeader> {

    Map<String, Object> saveOutboundNotice(List<OutboundNoticeParamDTO> dto);

    boolean insertOutboundNotice(OutboundNoticeHeader outboundNoticeHeader);

    /**
     * 根据通知单ID更新出库状态
     */
    boolean updateStatus(Long noticeId);

    List<OutboundNoticeHeaderDTO> selectHeaderList(Long houseId, String status, Integer size, Integer current);

    OutboundNoticeHeaderDTO selectHeaderDetail(Long houseId, Long id);

    /**
     * 查询出库通知单
     */
    Page<OutNoticeListResultDTO> queryOutboundNotice(Page<OutNoticeListResultDTO> page);

    /**
     * 出库通知导出
     */
    List<OutNoticeListResultDTO> queryExportONData(Map<String, String> condition);

}
