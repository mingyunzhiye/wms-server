package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.dto.opbaas.paramdto.AppCommonQueryDTO;
import com.zhiche.wms.dto.opbaas.paramdto.OrderReleaseParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ReleaseWithShipmentDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.dto.opbaas.resultdto.VechielModelDTO;

/**
 * <p>
 * 运单 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IOrderReleaseService extends IService<OtmOrderRelease> {

    /**
     * 运单指令查询
     */
    Page<OrderReleaseParamDTO> queryOrderReleaseList(Page<OrderReleaseParamDTO> page);

    /**
     * 装车交验 - 查询列表
     */
    Page<ReleaseWithShipmentDTO> queryReleaseShipList(Page<ReleaseWithShipmentDTO> page);

    /**
     * 装车交验-详情获取
     */
    ReleaseWithShipmentDTO getReleaseShipDetail(AppCommonQueryDTO dto);

    /**
     * 装车交验确认
     */
    OtmOrderRelease updateReleaseShip(AppCommonQueryDTO dto);

    /**
     * 装车发运 - 模糊搜索查询
     */
    Page<ShipmentForShipDTO> queryShipList(Page<ShipmentForShipDTO> page);

    /**
     * 装车发运 -- 点击/扫码获取详情
     */
    ShipmentForShipDTO getShipDetail(AppCommonQueryDTO dto);

    /**
     * 装车发运 -- 确认发运
     */
    ShipmentForShipDTO updateShip(AppCommonQueryDTO dto);

    /**
     * 快速赋码--绑定车架号  回传OTM
     */
    void updateVin(AppCommonQueryDTO dto);

    /**
     * 查询车型信息
     * @return
     * @param page
     */
    Page<VechielModelDTO> queryCarModelInfo (Page<VechielModelDTO> page);

    /**
     * 修改车型信息
     * @param conditions 修改条件
     */
    void updateVehicleInfo (AppCommonQueryDTO conditions);
}
