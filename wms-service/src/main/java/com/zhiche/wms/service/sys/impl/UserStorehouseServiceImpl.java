package com.zhiche.wms.service.sys.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.sys.UserStorehouseMapper;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.domain.model.sys.UserStorehouse;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;
import com.zhiche.wms.dto.sys.StorehouseDTO;
import com.zhiche.wms.service.base.IStoreLocationService;
import com.zhiche.wms.service.sys.IUserService;
import com.zhiche.wms.service.sys.IUserStorehouseService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 用户仓库权限表 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
@Service
public class UserStorehouseServiceImpl extends ServiceImpl<UserStorehouseMapper, UserStorehouse> implements IUserStorehouseService {

    @Autowired
    private IUserService userService;
    @Autowired
    private IStoreLocationService locationService;

    /**
     * 获取用户仓库权限
     */
    @Override
    public List<UserStorehouse> getUserHouses() {
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请先登录");
        }
        EntityWrapper<UserStorehouse> wrapper = new EntityWrapper<>();
        wrapper.eq("user_id", loginUser.getId());
        List<UserStorehouse> userStorehouses = selectList(wrapper);
        if (CollectionUtils.isEmpty(userStorehouses)) {
            throw new BaseException("未查询到用户" + loginUser.getName() + "关联仓库信息");
        }
        return userStorehouses;
    }

    /**
     * 查询所有库区库位
     */
    @Override
    public List<StoreAreaAndLocationDTO> queryAllArea(Map<String, String> condition) {
        if (Objects.isNull(condition) || condition.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("请先登录后操作");
        }
        String houseId = condition.get("houseId");
        if (StringUtils.isBlank(houseId)) {
            throw new BaseException("仓库id不能为空");
        }
        condition.put("status10", TableStatusEnum.STATUS_10.getCode());
        condition.put("orderBy", "b.id desc");
        return locationService.selectAllLocations(condition);
    }

}
