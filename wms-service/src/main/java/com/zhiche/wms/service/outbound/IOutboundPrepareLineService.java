package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareLine;
import com.zhiche.wms.dto.outbound.OutboundPrepareDTO;

import java.util.List;

/**
 * <p>
 * 出库备料单明细 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
public interface IOutboundPrepareLineService extends IService<OutboundPrepareLine> {
    List<OutboundPrepareDTO> queryPageByLotId(String key, String houseid, Integer current, Integer size);

    OutboundPrepareDTO getPrepare(String key,Long houseId);

    OutboundPrepareDTO getPrepareBykey(String key,Long houseId);

    OutboundPrepareDTO updateStatus(String key,Long houseId);
}
